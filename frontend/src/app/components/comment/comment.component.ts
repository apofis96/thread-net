import { Component, Input, OnDestroy } from '@angular/core';
import { Comment } from '../../models/comment/comment';
import { User } from '../../models/user';
import { catchError, switchMap, takeUntil } from 'rxjs/operators';
import { empty, Observable, Subject } from 'rxjs';
import { DialogType } from '../../models/common/auth-dialog-type';
import { LikeService } from '../../services/like.service';
import { AuthenticationService } from '../../services/auth.service';
import { AuthDialogService } from '../../services/auth-dialog.service';
import { CommentService } from '../../services/comment.service';
import { UpdateComment } from 'src/app/models/comment/update-comment';
import { SnackBarService } from 'src/app/services/snack-bar.service';

@Component({
    selector: 'app-comment',
    templateUrl: './comment.component.html',
    styleUrls: ['./comment.component.sass']
})
export class CommentComponent implements OnDestroy {
    @Input() public comment: Comment;
    @Input() public currentUser: User;

    public updateComment = {} as UpdateComment;
    private unsubscribe$ = new Subject<void>();
    public showCommentContainer = false;
    public show = true;

    public constructor(
        private authService: AuthenticationService,
        private authDialogService: AuthDialogService,
        private likeService: LikeService,
        private commentService: CommentService,
        private snackBarService: SnackBarService
    ) { }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    public likeComment(like: boolean) {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(
                    switchMap((userResp) => this.likeService.likeComment(this.comment, userResp, like)),
                    takeUntil(this.unsubscribe$)
                )
                .subscribe((comment) => (this.comment = comment));
            return;
        }

        this.likeService
            .likeComment(this.comment, this.currentUser, like)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((comment) => (this.comment = comment));
    }

    public likeCount() {
        return this.comment.reactions.filter(Reaction => Reaction.isLike).length;
    }
    public dislikeCount() {
        return this.comment.reactions.filter(Reaction => !Reaction.isLike).length;
    }
    private catchErrorWrapper(obs: Observable<User>) {
        return obs.pipe(
            catchError(() => {
                this.openAuthDialog();
                return empty();
            })
        );
    }
    public openAuthDialog() {
        this.authDialogService.openAuthDialog(DialogType.SignIn);
    }
    public toggleUpdateCommentContainer() {
        this.showCommentContainer = !this.showCommentContainer;
        if (this.showCommentContainer) {
            this.updateComment.body = this.comment.body;
        }
    }
    public sendComment() {
        this.updateComment.authorId = this.currentUser.id;
        this.updateComment.id = this.comment.id;
        this.commentService.updateComment(this.updateComment).pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {
                    if (resp) {
                        this.comment.body = resp.body.body;
                        this.updateComment.body = undefined;
                        this.showCommentContainer = false;
                    }
                },
                (error) => this.snackBarService.showErrorMessage(error)
            );
    }
    public deleteComment() {
        this.commentService.deleteComment(this.comment)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {
                    if (resp) {
                        this.show = false;
                    }
                },
                (error) => this.snackBarService.showErrorMessage(error)
            );
    }
}

