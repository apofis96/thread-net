import { Component, Input, Inject } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Reaction } from 'src/app/models/reactions/reaction';

@Component({
  selector: 'app-like-list',
  templateUrl: './like-list.component.html'
})
export class LikeListComponent {
  @Input() public reactions: Reaction[];
  constructor(public dialog: MatDialog) { }

  public openDialog() {
    this.dialog.open(DialogLikeListComponent, {
      width: '20em',
      data: this.reactions
    });
  }
}

@Component({
  selector: 'app-like-list-dialog',
  templateUrl: './like-list-dialog.component.html',
  styleUrls: ['./like-list-dialog.component.sass']
})
export class DialogLikeListComponent {
  constructor(@Inject(MAT_DIALOG_DATA) public data: Reaction[]) { }
  public likeCount() {
    return this.data.filter(reaction => reaction.isLike).length;
  }
  public dislikeCount() {
    return this.data.filter(reaction => !reaction.isLike).length;
  }
}

