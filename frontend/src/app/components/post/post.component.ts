import { Component, Input, OnDestroy, Inject } from '@angular/core';
import { Post } from '../../models/post/post';
import { AuthenticationService } from '../../services/auth.service';
import { AuthDialogService } from '../../services/auth-dialog.service';
import { empty, Observable, Subject } from 'rxjs';
import { DialogType } from '../../models/common/auth-dialog-type';
import { LikeService } from '../../services/like.service';
import { NewComment } from '../../models/comment/new-comment';
import { CommentService } from '../../services/comment.service';
import { User } from '../../models/user';
import { Comment } from '../../models/comment/comment';
import { catchError, switchMap, takeUntil } from 'rxjs/operators';
import { SnackBarService } from '../../services/snack-bar.service';
import { LikeListComponent } from '../like-list/like-list.component';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { UpdatePost } from '../../models/post/update-post';
import { PostService } from '../../services/post.service';
import { ImgurService } from '../../services/imgur.service';

@Component({
    selector: 'app-post',
    templateUrl: './post.component.html',
    styleUrls: ['./post.component.sass']
})
export class PostComponent implements OnDestroy {
    @Input() public post: Post;
    @Input() public currentUser: User;

    public imageUrl: string;
    public imageFile: File;
    public show = true;
    public loading = false;
    public showComments = false;
    public showPostContainer = false;
    public newComment = {} as NewComment;
    public updatePost = {} as UpdatePost;

    private unsubscribe$ = new Subject<void>();

    public constructor(
        private authService: AuthenticationService,
        private authDialogService: AuthDialogService,
        private likeService: LikeService,
        private commentService: CommentService,
        private snackBarService: SnackBarService,
        private postService: PostService,
        private imgurService: ImgurService,
        public dialog: MatDialog
    ) { }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    public toggleComments() {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(takeUntil(this.unsubscribe$))
                .subscribe((user) => {
                    if (user) {
                        this.currentUser = user;
                        this.showComments = !this.showComments;
                    }
                });
            return;
        }

        this.showComments = !this.showComments;
    }

    public likePost(like: boolean) {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(
                    switchMap((userResp) => this.likeService.likePost(this.post, userResp, like)),
                    takeUntil(this.unsubscribe$)
                )
                .subscribe((post) => (this.post = post));

            return;
        }

        this.likeService
            .likePost(this.post, this.currentUser, like)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((post) => (this.post = post));
    }

    public sendComment() {
        this.newComment.authorId = this.currentUser.id;
        this.newComment.postId = this.post.id;

        this.commentService
            .createComment(this.newComment)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {
                    if (resp) {
                        this.newComment.body = undefined;
                    }
                },
                (error) => this.snackBarService.showErrorMessage(error)
            );
    }

    public openAuthDialog() {
        this.authDialogService.openAuthDialog(DialogType.SignIn);
    }

    private catchErrorWrapper(obs: Observable<User>) {
        return obs.pipe(
            catchError(() => {
                this.openAuthDialog();
                return empty();
            })
        );
    }

    public likeCount() {
        return this.post.reactions.filter(Reaction => Reaction.isLike).length;
    }
    public dislikeCount() {
        return this.post.reactions.filter(Reaction => !Reaction.isLike).length;
    }
    public toggleUpdatePostContainer() {
        this.showPostContainer = !this.showPostContainer;
        if (this.showPostContainer) {
            this.imageUrl = this.post.previewImage;
            this.updatePost.body = this.post.body;
        }
    }
    public loadImage(target: any) {
        this.imageFile = target.files[0];

        if (!this.imageFile) {
            target.value = '';
            return;
        }

        if (this.imageFile.size / 1000000 > 5) {
            target.value = '';
            this.snackBarService.showErrorMessage(`Image can't be heavier than ~5MB`);
            return;
        }

        const reader = new FileReader();
        reader.addEventListener('load', () => (this.imageUrl = reader.result as string));
        reader.readAsDataURL(this.imageFile);
    }
    public removeImage() {
        this.imageUrl = undefined;
        this.imageFile = undefined;
    }
    public sendPost() {
        this.updatePost.authorId = this.currentUser.id;
        this.updatePost.id = this.post.id;
        const postSubscription = !this.imageFile
            ? this.postService.updatePost(this.updatePost)
            : this.imgurService.uploadToImgur(this.imageFile, 'title').pipe(
                switchMap((imageData) => {
                    this.updatePost.previewImage = imageData.body.data.link;
                    return this.postService.updatePost(this.updatePost);
                })
            );
        this.loading = true;
        postSubscription.pipe(takeUntil(this.unsubscribe$)).subscribe(
            (respPost) => {
                if (respPost) {
                    this.post.body = respPost.body.body;
                    this.post.previewImage = respPost.body.previewImage;
                }
                this.removeImage();
                this.updatePost.body = undefined;
                this.updatePost.previewImage = undefined;
                this.loading = false;
                this.showPostContainer = false;
            },
            (error) => this.snackBarService.showErrorMessage(error)
        );
    }
    public deletePost() {
        this.postService.deletePost(this.post)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {
                    if (resp) {
                        this.show = false;
                    }
                },
                (error) => this.snackBarService.showErrorMessage(error)
            );
    }
    public share() {
        this.dialog.open(DialogPostComponent, {
            width: '20em',
            data: { post: this.post, postService: this.postService, snackBarService: this.snackBarService }
        });
    }


}
@Component({
    selector: 'app-post-dialog',
    templateUrl: './post-dialog.component.html',
    styleUrls: ['./post.component.sass']
})
export class DialogPostComponent {

    public address: string;
    private unsubscribe$ = new Subject<void>();
    constructor(@Inject(MAT_DIALOG_DATA) public data: { post: Post, postService: PostService, snackBarService: SnackBarService }) { }
    public send() {
        this.data.postService.sharePost(this.data.post, this.address)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {
                },
                (error) => this.data.snackBarService.showErrorMessage(error)
            );;
    }
}
