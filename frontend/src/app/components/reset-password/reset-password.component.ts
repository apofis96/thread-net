import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription, Subject } from 'rxjs';
import { AuthenticationService } from '../../services/auth.service';
import { takeUntil } from 'rxjs/operators';
import { SnackBarService } from 'src/app/services/snack-bar.service';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.sass']
})
export class ResetPasswordComponent implements OnInit {

  private routeSubscription: Subscription;
  public hidePass = true;
  public token: string;
  public password: string;
  private unsubscribe$ = new Subject<void>();

  constructor(
    private route: ActivatedRoute,
    private authService: AuthenticationService,
    private router: Router,
    private snackBarService: SnackBarService
  ) {
    route.params.subscribe(params => this.token = params['token']);
  }

  ngOnInit(): void {
  }
  saveNewInfo() {
    const userSubscription = this.authService.updatePassword(this.password, this.token)
      .pipe(takeUntil(this.unsubscribe$)).subscribe(
        (resp) => {
          this.snackBarService.showUsualMessage('Successfully updated');
          this.router.navigateByUrl('/thread')
        },
        (error) => this.snackBarService.showErrorMessage(error)
      );
  }


}
