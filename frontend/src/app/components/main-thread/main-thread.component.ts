import { Component, OnInit, OnDestroy } from '@angular/core';
import { Post } from '../../models/post/post';
import { Comment } from '../../models/comment/comment';
import { User } from '../../models/user';
import { Subject } from 'rxjs';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { AuthenticationService } from '../../services/auth.service';
import { PostService } from '../../services/post.service';
import { AuthDialogService } from '../../services/auth-dialog.service';
import { DialogType } from '../../models/common/auth-dialog-type';
import { EventService } from '../../services/event.service';
import { ImgurService } from '../../services/imgur.service';
import { NewPost } from '../../models/post/new-post';
import { switchMap, takeUntil } from 'rxjs/operators';
import { HubConnectionBuilder, HubConnection } from '@aspnet/signalr';
import { SnackBarService } from '../../services/snack-bar.service';
import { ToastrService } from 'ngx-toastr';

@Component({
    selector: 'app-main-thread',
    templateUrl: './main-thread.component.html',
    styleUrls: ['./main-thread.component.sass']
})
export class MainThreadComponent implements OnInit, OnDestroy {
    public posts: Post[] = [];
    public cachedPosts: Post[] = [];
    public isOnlyMine = false;
    public isOnlyMineLike = false;

    public currentUser: User;
    public imageUrl: string;
    public imageFile: File;
    public post = {} as NewPost;
    public showPostContainer = false;
    public loading = false;
    public loadingPosts = false;

    public postHub: HubConnection;
    public commentHub: HubConnection;

    private unsubscribe$ = new Subject<void>();

    public constructor(
        private snackBarService: SnackBarService,
        private authService: AuthenticationService,
        private postService: PostService,
        private imgurService: ImgurService,
        private authDialogService: AuthDialogService,
        private eventService: EventService,
        private toastr: ToastrService
    ) { }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
        this.postHub.stop();
        this.commentHub.stop();
    }

    public ngOnInit() {
        this.registerHub();
        this.getPosts();
        this.getUser();

        this.eventService.userChangedEvent$.pipe(takeUntil(this.unsubscribe$)).subscribe((user) => {
            this.currentUser = user;
            this.post.authorId = this.currentUser ? this.currentUser.id : undefined;
        });
    }

    public getPosts() {
        this.loadingPosts = true;
        this.postService
            .getPosts()
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {
                    this.loadingPosts = false;
                    this.posts = this.cachedPosts = resp.body;
                },
                (error) => (this.loadingPosts = false)
            );
    }

    public sendPost() {
        const postSubscription = !this.imageFile
            ? this.postService.createPost(this.post)
            : this.imgurService.uploadToImgur(this.imageFile, 'title').pipe(
                switchMap((imageData) => {
                    this.post.previewImage = imageData.body.data.link;
                    return this.postService.createPost(this.post);
                })
            );

        this.loading = true;

        postSubscription.pipe(takeUntil(this.unsubscribe$)).subscribe(
            (respPost) => {
                this.addNewPost(respPost.body);
                this.removeImage();
                this.post.body = undefined;
                this.post.previewImage = undefined;
                this.loading = false;
            },
            (error) => this.snackBarService.showErrorMessage(error)
        );
    }

    public loadImage(target: any) {
        this.imageFile = target.files[0];

        if (!this.imageFile) {
            target.value = '';
            return;
        }

        if (this.imageFile.size / 1000000 > 5) {
            target.value = '';
            this.snackBarService.showErrorMessage(`Image can't be heavier than ~5MB`);
            return;
        }

        const reader = new FileReader();
        reader.addEventListener('load', () => (this.imageUrl = reader.result as string));
        reader.readAsDataURL(this.imageFile);
    }

    public removeImage() {
        this.imageUrl = undefined;
        this.imageFile = undefined;
    }
    public postSliderChanged(event: MatSlideToggleChange) {
        if (event.checked) {
            this.isOnlyMine = true;
            this.posts = this.posts.filter((x) => x.author.id === this.currentUser.id);
        } else {
            this.isOnlyMine = false;
            if (this.isOnlyMineLike) {
                this.posts = this.cachedPosts.filter((x) => x.reactions.some((y) => y.user.id === this.currentUser.id && y.isLike));
            }
            else {
                this.posts = this.cachedPosts;
            }
        }
    }
    public likeSliderChanged(event: MatSlideToggleChange) {
        if (event.checked) {
            this.isOnlyMineLike = true;
            this.posts = this.posts.filter((x) => x.reactions.some((y) => y.user.id === this.currentUser.id && y.isLike));
        } else {
            this.isOnlyMineLike = false;
            if (this.isOnlyMine) {
                this.posts = this.cachedPosts.filter((x) => x.author.id === this.currentUser.id);
            }
            else {
                this.posts = this.cachedPosts;
            }
        }
    }

    public toggleNewPostContainer() {
        this.showPostContainer = !this.showPostContainer;
    }

    public openAuthDialog() {
        this.authDialogService.openAuthDialog(DialogType.SignIn);
    }

    public registerHub() {
        this.commentHub = new HubConnectionBuilder().withUrl('https://localhost:44344/notifications/comment').build();
        this.postHub = new HubConnectionBuilder().withUrl('https://localhost:44344/notifications/post').build();
        this.postHub.start().catch((error) => this.snackBarService.showErrorMessage(error));
        this.commentHub.start().catch((error) => this.snackBarService.showErrorMessage(error));

        this.postHub.on('NewPost', (newPost: Post) => {
            if (newPost) {
                this.addNewPost(newPost);
            }
        });
        this.postHub.on('UpdatePost', (updatePost: Post) => {
            if (updatePost) {
                this.updatePost(updatePost);
            }
        });
        this.postHub.on('DeletePost', (deletePost: Post) => {
            if (deletePost) {
                this.deletePost(deletePost);
            }
        });
        this.postHub.on('UpdateReactions', (updatePost: Post) => {
            if (updatePost) {
                this.notifyLike(updatePost);
                this.updatePost(updatePost);
            }
        });

        this.commentHub.on('NewComment', (newCommentPost: Post) => {
            if (newCommentPost) {
                this.editCommentPost(newCommentPost);
            }
        });
        this.commentHub.on('UpdateComment', (updateCommentPost: Post) => {
            if (updateCommentPost) {
                this.editCommentPost(updateCommentPost);
            }
        });
        this.commentHub.on('DeleteComment', (deleteCommentPost: Post) => {
            if (deleteCommentPost) {
                this.editCommentPost(deleteCommentPost);
            }
        });
        this.commentHub.on('UpdateReactions', (updateCommentReactionsPost: Post) => {
            if (updateCommentReactionsPost) {
                this.editCommentPost(updateCommentReactionsPost);
            }
        });
    }

    public addNewPost(newPost: Post) {
        if (!this.cachedPosts.some((x) => x.id === newPost.id)) {
            this.cachedPosts = this.sortPostArray(this.cachedPosts.concat(newPost));
            if ((!this.isOnlyMine && !this.isOnlyMineLike) || (this.isOnlyMine && newPost.author.id === this.currentUser.id)) {
                this.posts = this.sortPostArray(this.posts.concat(newPost));
                if (newPost.author.id !== this.currentUser.id) {
                    this.showToast("New post!", newPost.author.userName + " has post something new!");
                }
            }
        }
    }
    public updatePost(updatePost: Post) {
        let id = this.cachedPosts.findIndex((x) => x.id === updatePost.id)
        this.cachedPosts[id].body = updatePost.body;
        this.cachedPosts[id].previewImage = updatePost.previewImage;
        this.cachedPosts[id].reactions = updatePost.reactions;
        if ((!this.isOnlyMine && !this.isOnlyMineLike) || (this.isOnlyMine && updatePost.author.id === this.currentUser.id) || (this.isOnlyMineLike && updatePost.reactions.some((x) => x.user.id === this.currentUser.id && x.isLike))) {
            id = this.posts.findIndex((x) => x.id === updatePost.id)
            this.posts[id].body = updatePost.body;
            this.posts[id].previewImage = updatePost.previewImage;
            this.posts[id].reactions = updatePost.reactions;
        }
    }
    public deletePost(deletePost: Post) {
        this.cachedPosts = this.cachedPosts.filter((x) => x.id !== deletePost.id);
        this.posts = this.posts.filter((x) => x.id !== deletePost.id);
    }
    public notifyLike(reactionPost: Post) {
        if (reactionPost.author.id === this.currentUser.id) {
            let cachedLikes = this.cachedPosts[this.cachedPosts.findIndex((x) => x.id === reactionPost.id)].reactions.filter((x) => x.isLike);
            let updateLikes = reactionPost.reactions.filter((x) => x.isLike);
            if (cachedLikes.length < updateLikes.length) {
                this.showToast("New Like!", updateLikes.filter(x => cachedLikes.indexOf(x) < 0)[0].user.userName + " has like your post!");
            }
        }
    }
    public editCommentPost(newCommentPost: Post) {
        this.cachedPosts.find((x) => x.id === newCommentPost.id).comments = this.sortCommentArray(newCommentPost.comments);
        if ((!this.isOnlyMine && !this.isOnlyMineLike) || (this.isOnlyMine && newCommentPost.author.id === this.currentUser.id) || (this.isOnlyMineLike && newCommentPost.reactions.some((x) => x.user.id === this.currentUser.id && x.isLike))) {
            this.posts.find((x) => x.id === newCommentPost.id).comments = this.sortCommentArray(newCommentPost.comments);
        }
    }

    private getUser() {
        this.authService
            .getUser()
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((user) => (this.currentUser = user));
    }

    private sortPostArray(array: Post[]): Post[] {
        return array.sort((a, b) => +new Date(b.createdAt) - +new Date(a.createdAt));
    }
    private sortCommentArray(array: Comment[]): Comment[] {
        return array.sort((a, b) => +new Date(a.createdAt) - +new Date(b.createdAt));
    }
    private showToast(title: string, message: string) {
        this.toastr.show(message, title,
            { timeOut: 5000 });;
    }
}
