export interface UpdateComment {
    id: number;
    authorId: number;
    body: string;
}
