﻿using AutoMapper;
using Microsoft.AspNetCore.SignalR;
using System.Linq;
using System.Threading.Tasks;
using Thread_.NET.BLL.Hubs;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.Like;
using Thread_.NET.DAL.Context;
using Thread_.NET.DAL.Entities;

namespace Thread_.NET.BLL.Services
{
    public sealed class LikeService : BaseService
    {
        private readonly MailService _mail;
        private readonly IHubContext<PostHub> _postHub;
        private readonly IHubContext<CommentHub> _commentHub;
        private readonly PostService _postService;

        public LikeService(ThreadContext context, IMapper mapper, MailService mail, IHubContext<PostHub> postHub, IHubContext<CommentHub> commentHub, PostService postService) : base(context, mapper)
        {
            _mail = mail;
            _postService = postService;
            _postHub = postHub;
            _commentHub = commentHub;
        }
        public async Task LikePost(NewReactionDTO reaction)
        {
            var likes = _context.PostReactions.Where(x => x.UserId == reaction.UserId && x.PostId == reaction.EntityId);
            if (likes.Any())
            {
                _context.PostReactions.RemoveRange(likes);
                await _context.SaveChangesAsync();
            }
            else
            {
                var newReaction = new PostReaction
                {
                    PostId = reaction.EntityId,
                    IsLike = reaction.IsLike,
                    UserId = reaction.UserId
                };
                _context.PostReactions.Add(newReaction);
                await _context.SaveChangesAsync();
                var post = _context.PostReactions.Where(x => x.UserId == reaction.UserId && x.PostId == reaction.EntityId).FirstOrDefault();
                await _mail.Send(post);
            }
            await _postHub.Clients.All.SendAsync("UpdateReactions", await _postService.GetPostById(reaction.EntityId));

        }
        public async Task LikeComment(NewReactionDTO reaction)
        {
            var likes = _context.CommentReactions.Where(x => x.UserId == reaction.UserId && x.CommentId == reaction.EntityId);
            if (likes.Any())
            {
                _context.CommentReactions.RemoveRange(likes);
                await _context.SaveChangesAsync();
            }
            else
            {
                _context.CommentReactions.Add(new CommentReaction
                {
                    CommentId = reaction.EntityId,
                    IsLike = reaction.IsLike,
                    UserId = reaction.UserId
                });
                await _context.SaveChangesAsync();
            }
            await _commentHub.Clients.All.SendAsync("UpdateReactions", await _postService.GetPostById(_context.Comments.FindAsync(reaction.EntityId).Result.PostId));
        }
    }
}
