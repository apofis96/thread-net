﻿using AutoMapper;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using Thread_.NET.BLL.Exceptions;
using Thread_.NET.BLL.Hubs;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.Comment;
using Thread_.NET.DAL.Context;
using Thread_.NET.DAL.Entities;

namespace Thread_.NET.BLL.Services
{
    public sealed class CommentService : BaseService
    {
        private readonly LikeService _likeService;
        private readonly IHubContext<CommentHub> _commentHub;
        private readonly PostService _postService;
        public CommentService(ThreadContext context, IMapper mapper, LikeService likeService, PostService postService, IHubContext<CommentHub> commentHub) : base(context, mapper) 
        {
            _postService = postService;
            _likeService = likeService;
            _commentHub = commentHub;
        }

        public async Task<CommentDTO> CreateComment(NewCommentDTO newComment)
        {
            var commentEntity = _mapper.Map<Comment>(newComment);

            _context.Comments.Add(commentEntity);
            await _context.SaveChangesAsync();

            var createdComment = await _context.Comments
                .Include(comment => comment.Author)
                    .ThenInclude(user => user.Avatar)
                .FirstAsync(comment => comment.Id == commentEntity.Id);
            var comment = _mapper.Map<CommentDTO>(createdComment);
            await _commentHub.Clients.All.SendAsync("NewComment", await _postService.GetPostById(createdComment.PostId));
            return _mapper.Map<CommentDTO>(comment);
        }
        public async Task<CommentDTO> UpdateComment(UpdateCommentDTO commentDto)
        {
            var commentEntity = _context.Comments.Find(commentDto.Id);
            if (commentEntity.AuthorId != commentDto.AuthorId)
            {
                throw new InvalidOperationException("Bad userID");
            }
            commentEntity.UpdatedAt = DateTime.Now;
            commentEntity.Body = commentDto.Body;
            _context.Comments.Update(commentEntity);
            await _context.SaveChangesAsync();
            var updatedCommentDTO = _mapper.Map<CommentDTO>(commentEntity);
            await _commentHub.Clients.All.SendAsync("UpdateComment", await _postService.GetPostById(commentEntity.PostId));
            return updatedCommentDTO;
        }
        public async Task DeleteComment(int commentId, int userId)
        {
            var commentEntity = await _context.Comments.FirstOrDefaultAsync(comment => comment.Id == commentId);
            if (commentEntity == null)
            {
                throw new NotFoundException(nameof(Comment), commentId);
            }
            if (commentEntity.AuthorId != userId)
            {
                throw new InvalidOperationException("Bad userID");
            }
            var post = await _postService.GetPostById(commentEntity.PostId);
            post.Comments = post.Comments.Where(x => x.Id != commentId).ToList();
            commentEntity.IsDeleted = true;
            _context.Comments.Update(commentEntity);
            await _context.SaveChangesAsync();
            await _commentHub.Clients.All.SendAsync("DeleteComment", post);
        }
    }
}
