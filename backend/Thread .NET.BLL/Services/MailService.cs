﻿using System.Net;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using MailKit.Net.Smtp;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using MimeKit;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.Mail;
using Thread_.NET.DAL.Context;
using Thread_.NET.DAL.Entities;

namespace Thread_.NET.BLL.Services
{
	public class MailService : BaseService
	{
		private readonly MailOptions _mailOptions;

		public MailService(ThreadContext context, IMapper mapper, IOptions<MailOptions> mailOptions) : base(context, mapper)
		{
			_mailOptions = mailOptions.Value;
		}
		public async Task Send(PostReaction reaction)
		{
			var r = _context.PostReactions.Include(reaction => reaction.Post)
					.ThenInclude(post => post.Author)
				.Include(reaction => reaction.User)
				.Where(p => p.Id == reaction.Id)
				.FirstOrDefault();
			var message = new MimeMessage();
			message.To.Add(new MailboxAddress("", r.Post.Author.Email));
			message.Subject = "New reaction to your Post!";
			var bodyBuilder = new BodyBuilder();
			bodyBuilder.HtmlBody = "<p>Your post has been rated by <b>" + r.User.UserName+ "</b>!</p>";
			bodyBuilder.TextBody = "<Your post has been rated by " + r.User.UserName + "!";
			message.Body = bodyBuilder.ToMessageBody();
			await Send(message);

		}
		public async Task Send(int postId, string address)
        {
			var post = _context.Posts.Include(p => p.Author)
					.ThenInclude(a => a.Avatar)
				.Include(p => p.Preview)
				.Where(p => p.Id == postId)
				.FirstOrDefault();
			var message = new MimeMessage();
			message.To.Add(new MailboxAddress("", address));
			var bodyBuilder = new BodyBuilder();
			message.Subject = "Someone share Post to You!";
			bodyBuilder.HtmlBody = string.Format(@"<img src=""{0}"" alt=""avatar"" width=""60"" style=""border-radius: 50 %;""><span>{1}</span><br><p>{2}</p><br>", post.Author.Avatar.URL, post.Author.UserName,post.Body);
			if(post.PreviewId != null)
            {
				bodyBuilder.HtmlBody += string.Format(@"<img src=""{0}"" alt=""preview"" width=""600"">", post.Preview.URL);
            }
			message.Body = bodyBuilder.ToMessageBody();
			await Send(message);
		}
		public async Task Send(string address, string url, string token)
		{
			var message = new MimeMessage();
			message.To.Add(new MailboxAddress("", address));
			var bodyBuilder = new BodyBuilder();
			message.Subject = "Reset password";
			bodyBuilder.HtmlBody = string.Format(@"<a href=""http://{0}/reset/{1}"">Click to reset</a>", url, WebUtility.UrlEncode(token));
			message.Body = bodyBuilder.ToMessageBody();
			await Send(message);
		}
		private async Task Send(MimeMessage message)
        {
			message.From.Add(new MailboxAddress("", _mailOptions.SmtpAddress));
			using (var emailClient = new SmtpClient())
			{
				await emailClient.ConnectAsync(_mailOptions.SmtpServer, _mailOptions.SmtpPort, true);
				emailClient.AuthenticationMechanisms.Remove("XOAUTH2");
				await emailClient.AuthenticateAsync(_mailOptions.SmtpUsername, _mailOptions.SmtpPassword);
				await emailClient.SendAsync(message);
				await emailClient.DisconnectAsync(true);
			}
		}

	}
}
