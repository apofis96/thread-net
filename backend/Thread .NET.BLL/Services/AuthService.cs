﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using Thread_.NET.BLL.Exceptions;
using Thread_.NET.BLL.JWT;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.Auth;
using Thread_.NET.Common.DTO.User;
using Thread_.NET.Common.Security;
using Thread_.NET.DAL.Context;
using Thread_.NET.DAL.Entities;

namespace Thread_.NET.BLL.Services
{
    public sealed class AuthService : BaseService
    {
        private readonly JwtFactory _jwtFactory;
        private readonly MailService _mailService;

        public AuthService(ThreadContext context, IMapper mapper, JwtFactory jwtFactory, MailService mailService) : base(context, mapper)
        {
            _jwtFactory = jwtFactory;
            _mailService = mailService;
        }

        public async Task<AuthUserDTO> Authorize(UserLoginDTO userDto)
        {
            var userEntity = await _context.Users
                .Include(u => u.Avatar)
                .FirstOrDefaultAsync(u => u.Email == userDto.Email);

            if (userEntity == null)
            {
                throw new NotFoundException(nameof(User));
            }

            if (!SecurityHelper.ValidatePassword(userDto.Password, userEntity.Password, userEntity.Salt))
            {
                throw new InvalidUsernameOrPasswordException();
            }

            var token = await GenerateAccessToken(userEntity.Id, userEntity.UserName, userEntity.Email);
            var user = _mapper.Map<UserDTO>(userEntity);

            return new AuthUserDTO
            {
                User = user,
                Token = token
            };
        }

        public async Task<AccessTokenDTO> GenerateAccessToken(int userId, string userName, string email)
        {
            var refreshToken = _jwtFactory.GenerateRefreshToken();

            _context.RefreshTokens.Add(new RefreshToken
            {
                Token = refreshToken,
                UserId = userId
            });

            await _context.SaveChangesAsync();

            var accessToken = await _jwtFactory.GenerateAccessToken(userId, userName, email);

            return new AccessTokenDTO(accessToken, refreshToken);
        }

        public async Task<AccessTokenDTO> RefreshToken(RefreshTokenDTO dto)
        {
            var userId = _jwtFactory.GetUserIdFromToken(dto.AccessToken, dto.SigningKey);
            var userEntity = await _context.Users.FindAsync(userId);

            if (userEntity == null)
            {
                throw new NotFoundException(nameof(User), userId);
            }

            var rToken = await _context.RefreshTokens.FirstOrDefaultAsync(t => t.Token == dto.RefreshToken && t.UserId == userId);

            if (rToken == null)
            {
                throw new InvalidTokenException("refresh");
            }

            if (!rToken.IsActive)
            {
                throw new ExpiredRefreshTokenException();
            }

            var jwtToken = await _jwtFactory.GenerateAccessToken(userEntity.Id, userEntity.UserName, userEntity.Email);
            var refreshToken = _jwtFactory.GenerateRefreshToken();

            _context.RefreshTokens.Remove(rToken); // delete the token we've exchanged
            _context.RefreshTokens.Add(new RefreshToken // add the new one
            {
                Token = refreshToken,
                UserId = userEntity.Id
            });

            await _context.SaveChangesAsync();

            return new AccessTokenDTO(jwtToken, refreshToken);
        }

        public async Task RevokeRefreshToken(string refreshToken, int userId)
        {
            var rToken = _context.RefreshTokens.FirstOrDefault(t => t.Token == refreshToken && t.UserId == userId);

            if (rToken == null)
            {
                throw new InvalidTokenException("refresh");
            }

            _context.RefreshTokens.Remove(rToken);
            await _context.SaveChangesAsync();
        }

        public async Task Reset(string address, string url)
        {
            var userEntity = await _context.Users.Where(u => u.Email == address).FirstOrDefaultAsync();
            if (userEntity != null)
            {
                var refreshToken = _jwtFactory.GenerateRefreshToken();
                _context.ResetToken.Add(new ResetToken // add the new one
                {
                    Token = refreshToken,
                    UserId = userEntity.Id

                });
                await _context.SaveChangesAsync();
                await _mailService.Send(address, url, refreshToken);
            }
        }
        public async Task Update(string password, string token)
        {
            if (password.Length < 4 || password.Length > 16)
            {
                throw new ArgumentException("Password must be from 4 to 16 characters.");
            }
            var resetToken = await _context.ResetToken.FirstOrDefaultAsync(t => t.Token == token);
            if (resetToken == null)
            {
                throw new InvalidTokenException("InvalidToken");
            }
            if (!resetToken.IsActive)
            {
                _context.ResetToken.Remove(resetToken);
                await _context.SaveChangesAsync();
                throw new ExpiredRefreshTokenException();
            }
            var userEntity = await _context.Users.FindAsync(resetToken.UserId);
            var salt = SecurityHelper.GetRandomBytes();
            userEntity.Salt = Convert.ToBase64String(salt);
            userEntity.Password = SecurityHelper.HashPassword(password, salt);
            _context.Users.Update(userEntity);
            _context.ResetToken.RemoveRange(_context.ResetToken.Where(t => t.UserId == userEntity.Id));
            _context.RefreshTokens.RemoveRange(_context.RefreshTokens.Where(t => t.UserId == userEntity.Id));
            await _context.SaveChangesAsync();
        }
    }
}
