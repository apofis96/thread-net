﻿using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;
using Thread_.NET.Common.DTO.Post;

namespace Thread_.NET.BLL.Hubs
{
    public sealed class CommentHub : Hub
    {
        public async Task Send(PostDTO comnent)
        {
            await Clients.All.SendAsync("NewComment", comnent);
        }
    }
}
