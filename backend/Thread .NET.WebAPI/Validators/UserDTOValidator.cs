﻿using FluentValidation;
using Microsoft.Extensions.DependencyInjection;
using System;
using Thread_.NET.BLL.Services;
using Thread_.NET.Common.DTO.User;

namespace Thread_.NET.Validators
{
    public sealed class UserRegisterDTOValidator : AbstractValidator<UserRegisterDTO>
    {
        private readonly IServiceProvider _serviceProvider;

        public UserRegisterDTOValidator(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;

            RuleFor(u => u.UserName)
                .NotEmpty()
                    .WithMessage("Username is mandatory.")
                .MinimumLength(3)
                    .WithMessage("Username should be minimum 3 character.");

            RuleFor(u => u.Email)
                .EmailAddress()
                .Must(IsEmailUniqu).WithMessage("Email already taken");

            RuleFor(u => u.Password)
                .Length(4, 16)
                .WithMessage("Password must be from 4 to 16 characters.");

        }
        private bool IsEmailUniqu(string email)
        {
            using (var serviceScope = _serviceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                var ser = serviceScope.ServiceProvider.GetService<UserService>();
                return ser.IsEmailUniqu(email);

            }

        }
    }
    public sealed class UserUpdateDTOValidator : AbstractValidator<UserDTO>
    {
        private readonly IServiceProvider _serviceProvider;

        public UserUpdateDTOValidator(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;

            RuleFor(u => u.UserName)
                .NotEmpty()
                    .WithMessage("Username is mandatory.")
                .MinimumLength(3)
                    .WithMessage("Username should be minimum 3 character.");

            RuleFor(u => u.Email)
                .EmailAddress()
                .Must((o, email) => { return IsEmailUniqu(email, o.Id); }).WithMessage("Email already taken");

        }
        private bool IsEmailUniqu(string email, int id)
        {
            using (var serviceScope = _serviceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                var ser = serviceScope.ServiceProvider.GetService<UserService>();
                var user = ser.GetUserById(id).Result;
                if (user.Email != email)
                {
                    return ser.IsEmailUniqu(email);
                }
                return true;
            }

        }
    }
}
