﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Thread_.NET.BLL.Services;
using Thread_.NET.Common.DTO.User;

namespace Thread_.NET.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [AllowAnonymous]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly AuthService _authService;

        public AuthController(AuthService authService)
        {
            _authService = authService;
        }

        [HttpPost("login")]
        public async Task<ActionResult<AuthUserDTO>> Login(UserLoginDTO dto)
        {
            return Ok(await _authService.Authorize(dto));
        }
        [HttpPost("reset/{email}")]
        public async Task<IActionResult> Reset(string email, string host)
        {
            await _authService.Reset(email, host);
            return Ok();
        }
        [HttpPut("update/{password}")]
        public async Task<IActionResult> Update(string password, string token)
        {
            await _authService.Update(password, token);
            return Ok();
        }
    }
}