﻿namespace Thread_.NET.DAL.Entities.Abstract
{
    interface ISoftDelete
    {
        bool IsDeleted { get; set; }
    }
}
