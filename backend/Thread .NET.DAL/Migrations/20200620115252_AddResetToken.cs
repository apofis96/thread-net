﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Thread_.NET.DAL.Migrations
{
    public partial class AddResetToken : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.CreateTable(
                name: "ResetToken",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: false),
                    Token = table.Column<string>(nullable: true),
                    Expires = table.Column<DateTime>(nullable: false),
                    UserId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ResetToken", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ResetToken_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "CommentReactions",
                columns: new[] { "Id", "CommentId", "CreatedAt", "IsLike", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 20, 17, new DateTime(2020, 6, 20, 14, 52, 48, 780, DateTimeKind.Local).AddTicks(1186), false, new DateTime(2020, 6, 20, 14, 52, 48, 780, DateTimeKind.Local).AddTicks(1220), 1 },
                    { 2, 3, new DateTime(2020, 6, 20, 14, 52, 48, 779, DateTimeKind.Local).AddTicks(6983), false, new DateTime(2020, 6, 20, 14, 52, 48, 779, DateTimeKind.Local).AddTicks(7219), 2 },
                    { 3, 13, new DateTime(2020, 6, 20, 14, 52, 48, 779, DateTimeKind.Local).AddTicks(7467), true, new DateTime(2020, 6, 20, 14, 52, 48, 779, DateTimeKind.Local).AddTicks(7506), 4 },
                    { 4, 5, new DateTime(2020, 6, 20, 14, 52, 48, 779, DateTimeKind.Local).AddTicks(7694), false, new DateTime(2020, 6, 20, 14, 52, 48, 779, DateTimeKind.Local).AddTicks(7731), 4 },
                    { 5, 16, new DateTime(2020, 6, 20, 14, 52, 48, 779, DateTimeKind.Local).AddTicks(7913), false, new DateTime(2020, 6, 20, 14, 52, 48, 779, DateTimeKind.Local).AddTicks(7953), 2 },
                    { 6, 8, new DateTime(2020, 6, 20, 14, 52, 48, 779, DateTimeKind.Local).AddTicks(8135), false, new DateTime(2020, 6, 20, 14, 52, 48, 779, DateTimeKind.Local).AddTicks(8174), 10 },
                    { 7, 13, new DateTime(2020, 6, 20, 14, 52, 48, 779, DateTimeKind.Local).AddTicks(8349), false, new DateTime(2020, 6, 20, 14, 52, 48, 779, DateTimeKind.Local).AddTicks(8388), 20 },
                    { 8, 13, new DateTime(2020, 6, 20, 14, 52, 48, 779, DateTimeKind.Local).AddTicks(8571), false, new DateTime(2020, 6, 20, 14, 52, 48, 779, DateTimeKind.Local).AddTicks(8606), 16 },
                    { 9, 19, new DateTime(2020, 6, 20, 14, 52, 48, 779, DateTimeKind.Local).AddTicks(8782), false, new DateTime(2020, 6, 20, 14, 52, 48, 779, DateTimeKind.Local).AddTicks(8817), 6 },
                    { 10, 12, new DateTime(2020, 6, 20, 14, 52, 48, 779, DateTimeKind.Local).AddTicks(8995), false, new DateTime(2020, 6, 20, 14, 52, 48, 779, DateTimeKind.Local).AddTicks(9032), 3 },
                    { 1, 6, new DateTime(2020, 6, 20, 14, 52, 48, 779, DateTimeKind.Local).AddTicks(1079), true, new DateTime(2020, 6, 20, 14, 52, 48, 779, DateTimeKind.Local).AddTicks(4297), 19 },
                    { 12, 20, new DateTime(2020, 6, 20, 14, 52, 48, 779, DateTimeKind.Local).AddTicks(9432), true, new DateTime(2020, 6, 20, 14, 52, 48, 779, DateTimeKind.Local).AddTicks(9470), 1 },
                    { 13, 2, new DateTime(2020, 6, 20, 14, 52, 48, 779, DateTimeKind.Local).AddTicks(9646), true, new DateTime(2020, 6, 20, 14, 52, 48, 779, DateTimeKind.Local).AddTicks(9682), 3 },
                    { 14, 17, new DateTime(2020, 6, 20, 14, 52, 48, 779, DateTimeKind.Local).AddTicks(9858), true, new DateTime(2020, 6, 20, 14, 52, 48, 779, DateTimeKind.Local).AddTicks(9894), 9 },
                    { 15, 19, new DateTime(2020, 6, 20, 14, 52, 48, 780, DateTimeKind.Local).AddTicks(73), false, new DateTime(2020, 6, 20, 14, 52, 48, 780, DateTimeKind.Local).AddTicks(109), 14 },
                    { 16, 12, new DateTime(2020, 6, 20, 14, 52, 48, 780, DateTimeKind.Local).AddTicks(283), false, new DateTime(2020, 6, 20, 14, 52, 48, 780, DateTimeKind.Local).AddTicks(319), 12 },
                    { 17, 11, new DateTime(2020, 6, 20, 14, 52, 48, 780, DateTimeKind.Local).AddTicks(496), false, new DateTime(2020, 6, 20, 14, 52, 48, 780, DateTimeKind.Local).AddTicks(533), 5 },
                    { 18, 14, new DateTime(2020, 6, 20, 14, 52, 48, 780, DateTimeKind.Local).AddTicks(707), false, new DateTime(2020, 6, 20, 14, 52, 48, 780, DateTimeKind.Local).AddTicks(744), 8 },
                    { 19, 3, new DateTime(2020, 6, 20, 14, 52, 48, 780, DateTimeKind.Local).AddTicks(964), false, new DateTime(2020, 6, 20, 14, 52, 48, 780, DateTimeKind.Local).AddTicks(1004), 10 },
                    { 11, 6, new DateTime(2020, 6, 20, 14, 52, 48, 779, DateTimeKind.Local).AddTicks(9211), true, new DateTime(2020, 6, 20, 14, 52, 48, 779, DateTimeKind.Local).AddTicks(9247), 9 }
                });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 10, "Perferendis ut sunt enim expedita distinctio sunt sunt voluptatum.", new DateTime(2020, 6, 20, 14, 52, 48, 727, DateTimeKind.Local).AddTicks(7329), 3, new DateTime(2020, 6, 20, 14, 52, 48, 728, DateTimeKind.Local).AddTicks(832) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 12, "Ex quae repellendus eos autem labore est.", new DateTime(2020, 6, 20, 14, 52, 48, 728, DateTimeKind.Local).AddTicks(3590), 12, new DateTime(2020, 6, 20, 14, 52, 48, 728, DateTimeKind.Local).AddTicks(3711) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 4, "Vero culpa eligendi doloremque assumenda consequatur.", new DateTime(2020, 6, 20, 14, 52, 48, 728, DateTimeKind.Local).AddTicks(4199), 15, new DateTime(2020, 6, 20, 14, 52, 48, 728, DateTimeKind.Local).AddTicks(4241) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 2, "Magnam nulla rerum et.", new DateTime(2020, 6, 20, 14, 52, 48, 728, DateTimeKind.Local).AddTicks(4597), 4, new DateTime(2020, 6, 20, 14, 52, 48, 728, DateTimeKind.Local).AddTicks(4644) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 7, "Excepturi et corrupti cum voluptatem sit et enim dolore.", new DateTime(2020, 6, 20, 14, 52, 48, 728, DateTimeKind.Local).AddTicks(5377), 12, new DateTime(2020, 6, 20, 14, 52, 48, 728, DateTimeKind.Local).AddTicks(5427) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 12, "Porro non est vel dolore.", new DateTime(2020, 6, 20, 14, 52, 48, 728, DateTimeKind.Local).AddTicks(5793), 14, new DateTime(2020, 6, 20, 14, 52, 48, 728, DateTimeKind.Local).AddTicks(5839) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 13, "Culpa fuga saepe minima itaque assumenda modi officiis nobis velit.", new DateTime(2020, 6, 20, 14, 52, 48, 728, DateTimeKind.Local).AddTicks(6253), 15, new DateTime(2020, 6, 20, 14, 52, 48, 728, DateTimeKind.Local).AddTicks(6297) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 9, "Occaecati est aspernatur veritatis repudiandae iusto voluptas ratione.", new DateTime(2020, 6, 20, 14, 52, 48, 728, DateTimeKind.Local).AddTicks(6682), 16, new DateTime(2020, 6, 20, 14, 52, 48, 728, DateTimeKind.Local).AddTicks(6728) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 15, "Magnam temporibus doloribus veniam non aperiam.", new DateTime(2020, 6, 20, 14, 52, 48, 728, DateTimeKind.Local).AddTicks(7134), 18, new DateTime(2020, 6, 20, 14, 52, 48, 728, DateTimeKind.Local).AddTicks(7180) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 6, "Dolor ad rerum id non quod.", new DateTime(2020, 6, 20, 14, 52, 48, 728, DateTimeKind.Local).AddTicks(7546), 16, new DateTime(2020, 6, 20, 14, 52, 48, 728, DateTimeKind.Local).AddTicks(7590) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 14, "Fuga quo eligendi fugiat cum adipisci sed.", new DateTime(2020, 6, 20, 14, 52, 48, 728, DateTimeKind.Local).AddTicks(7960), 16, new DateTime(2020, 6, 20, 14, 52, 48, 728, DateTimeKind.Local).AddTicks(8002) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 10, "Consequuntur voluptas nulla velit voluptas.", new DateTime(2020, 6, 20, 14, 52, 48, 728, DateTimeKind.Local).AddTicks(8521), 1, new DateTime(2020, 6, 20, 14, 52, 48, 728, DateTimeKind.Local).AddTicks(8566) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 15, "Omnis earum ut et.", new DateTime(2020, 6, 20, 14, 52, 48, 728, DateTimeKind.Local).AddTicks(8921), 15, new DateTime(2020, 6, 20, 14, 52, 48, 728, DateTimeKind.Local).AddTicks(8963) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "UpdatedAt" },
                values: new object[] { 4, "Quas dolore voluptas commodi at.", new DateTime(2020, 6, 20, 14, 52, 48, 728, DateTimeKind.Local).AddTicks(9315), new DateTime(2020, 6, 20, 14, 52, 48, 728, DateTimeKind.Local).AddTicks(9359) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { "Deserunt possimus rerum.", new DateTime(2020, 6, 20, 14, 52, 48, 728, DateTimeKind.Local).AddTicks(9724), 9, new DateTime(2020, 6, 20, 14, 52, 48, 728, DateTimeKind.Local).AddTicks(9769) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 6, "Quibusdam beatae eveniet.", new DateTime(2020, 6, 20, 14, 52, 48, 729, DateTimeKind.Local).AddTicks(111), 8, new DateTime(2020, 6, 20, 14, 52, 48, 729, DateTimeKind.Local).AddTicks(154) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 18, "Sed suscipit beatae quae corporis sit deserunt voluptatum.", new DateTime(2020, 6, 20, 14, 52, 48, 729, DateTimeKind.Local).AddTicks(524), 5, new DateTime(2020, 6, 20, 14, 52, 48, 729, DateTimeKind.Local).AddTicks(567) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "UpdatedAt" },
                values: new object[] { 17, "Quia rerum atque ipsum hic commodi ut eveniet neque earum.", new DateTime(2020, 6, 20, 14, 52, 48, 729, DateTimeKind.Local).AddTicks(957), new DateTime(2020, 6, 20, 14, 52, 48, 729, DateTimeKind.Local).AddTicks(999) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 10, "Saepe libero est aperiam voluptatibus eos.", new DateTime(2020, 6, 20, 14, 52, 48, 729, DateTimeKind.Local).AddTicks(1346), 8, new DateTime(2020, 6, 20, 14, 52, 48, 729, DateTimeKind.Local).AddTicks(1390) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 2, "Alias consequatur ipsum ipsum quibusdam.", new DateTime(2020, 6, 20, 14, 52, 48, 729, DateTimeKind.Local).AddTicks(1736), 14, new DateTime(2020, 6, 20, 14, 52, 48, 729, DateTimeKind.Local).AddTicks(1781) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 20, 14, 52, 48, 79, DateTimeKind.Local).AddTicks(7262), "https://s3.amazonaws.com/uifaces/faces/twitter/dactrtr/128.jpg", new DateTime(2020, 6, 20, 14, 52, 48, 83, DateTimeKind.Local).AddTicks(4736) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 20, 14, 52, 48, 83, DateTimeKind.Local).AddTicks(9324), "https://s3.amazonaws.com/uifaces/faces/twitter/akashsharma39/128.jpg", new DateTime(2020, 6, 20, 14, 52, 48, 83, DateTimeKind.Local).AddTicks(9680) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 20, 14, 52, 48, 84, DateTimeKind.Local).AddTicks(463), "https://s3.amazonaws.com/uifaces/faces/twitter/perfectflow/128.jpg", new DateTime(2020, 6, 20, 14, 52, 48, 84, DateTimeKind.Local).AddTicks(508) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 20, 14, 52, 48, 84, DateTimeKind.Local).AddTicks(721), "https://s3.amazonaws.com/uifaces/faces/twitter/coreyweb/128.jpg", new DateTime(2020, 6, 20, 14, 52, 48, 84, DateTimeKind.Local).AddTicks(764) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 20, 14, 52, 48, 84, DateTimeKind.Local).AddTicks(950), "https://s3.amazonaws.com/uifaces/faces/twitter/imsoper/128.jpg", new DateTime(2020, 6, 20, 14, 52, 48, 84, DateTimeKind.Local).AddTicks(992) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 20, 14, 52, 48, 84, DateTimeKind.Local).AddTicks(1178), "https://s3.amazonaws.com/uifaces/faces/twitter/thewillbeard/128.jpg", new DateTime(2020, 6, 20, 14, 52, 48, 84, DateTimeKind.Local).AddTicks(1221) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 20, 14, 52, 48, 84, DateTimeKind.Local).AddTicks(1412), "https://s3.amazonaws.com/uifaces/faces/twitter/fiterik/128.jpg", new DateTime(2020, 6, 20, 14, 52, 48, 84, DateTimeKind.Local).AddTicks(1454) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 20, 14, 52, 48, 84, DateTimeKind.Local).AddTicks(1640), "https://s3.amazonaws.com/uifaces/faces/twitter/mashaaaaal/128.jpg", new DateTime(2020, 6, 20, 14, 52, 48, 84, DateTimeKind.Local).AddTicks(1683) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 20, 14, 52, 48, 84, DateTimeKind.Local).AddTicks(1867), "https://s3.amazonaws.com/uifaces/faces/twitter/erwanhesry/128.jpg", new DateTime(2020, 6, 20, 14, 52, 48, 84, DateTimeKind.Local).AddTicks(1911) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 20, 14, 52, 48, 84, DateTimeKind.Local).AddTicks(2096), "https://s3.amazonaws.com/uifaces/faces/twitter/plbabin/128.jpg", new DateTime(2020, 6, 20, 14, 52, 48, 84, DateTimeKind.Local).AddTicks(2136) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 20, 14, 52, 48, 84, DateTimeKind.Local).AddTicks(2323), "https://s3.amazonaws.com/uifaces/faces/twitter/tanveerrao/128.jpg", new DateTime(2020, 6, 20, 14, 52, 48, 84, DateTimeKind.Local).AddTicks(2366) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 20, 14, 52, 48, 84, DateTimeKind.Local).AddTicks(2551), "https://s3.amazonaws.com/uifaces/faces/twitter/kinday/128.jpg", new DateTime(2020, 6, 20, 14, 52, 48, 84, DateTimeKind.Local).AddTicks(2591) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 20, 14, 52, 48, 84, DateTimeKind.Local).AddTicks(2777), "https://s3.amazonaws.com/uifaces/faces/twitter/arpitnj/128.jpg", new DateTime(2020, 6, 20, 14, 52, 48, 84, DateTimeKind.Local).AddTicks(2817) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 20, 14, 52, 48, 84, DateTimeKind.Local).AddTicks(2999), "https://s3.amazonaws.com/uifaces/faces/twitter/mkginfo/128.jpg", new DateTime(2020, 6, 20, 14, 52, 48, 84, DateTimeKind.Local).AddTicks(3043) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 20, 14, 52, 48, 84, DateTimeKind.Local).AddTicks(3226), "https://s3.amazonaws.com/uifaces/faces/twitter/antonyzotov/128.jpg", new DateTime(2020, 6, 20, 14, 52, 48, 84, DateTimeKind.Local).AddTicks(3270) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 20, 14, 52, 48, 84, DateTimeKind.Local).AddTicks(3456), "https://s3.amazonaws.com/uifaces/faces/twitter/justinrhee/128.jpg", new DateTime(2020, 6, 20, 14, 52, 48, 84, DateTimeKind.Local).AddTicks(3497) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 20, 14, 52, 48, 84, DateTimeKind.Local).AddTicks(3686), "https://s3.amazonaws.com/uifaces/faces/twitter/bereto/128.jpg", new DateTime(2020, 6, 20, 14, 52, 48, 84, DateTimeKind.Local).AddTicks(3728) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 20, 14, 52, 48, 84, DateTimeKind.Local).AddTicks(3916), "https://s3.amazonaws.com/uifaces/faces/twitter/ludwiczakpawel/128.jpg", new DateTime(2020, 6, 20, 14, 52, 48, 84, DateTimeKind.Local).AddTicks(3956) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 20, 14, 52, 48, 84, DateTimeKind.Local).AddTicks(4206), "https://s3.amazonaws.com/uifaces/faces/twitter/leonfedotov/128.jpg", new DateTime(2020, 6, 20, 14, 52, 48, 84, DateTimeKind.Local).AddTicks(4250) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 20, 14, 52, 48, 84, DateTimeKind.Local).AddTicks(4434), "https://s3.amazonaws.com/uifaces/faces/twitter/kaspernordkvist/128.jpg", new DateTime(2020, 6, 20, 14, 52, 48, 84, DateTimeKind.Local).AddTicks(4480) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 20, 14, 52, 48, 103, DateTimeKind.Local).AddTicks(7125), "https://picsum.photos/640/480/?image=200", new DateTime(2020, 6, 20, 14, 52, 48, 104, DateTimeKind.Local).AddTicks(61) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 20, 14, 52, 48, 104, DateTimeKind.Local).AddTicks(1228), "https://picsum.photos/640/480/?image=794", new DateTime(2020, 6, 20, 14, 52, 48, 104, DateTimeKind.Local).AddTicks(1345) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 20, 14, 52, 48, 104, DateTimeKind.Local).AddTicks(1623), "https://picsum.photos/640/480/?image=58", new DateTime(2020, 6, 20, 14, 52, 48, 104, DateTimeKind.Local).AddTicks(1670) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 20, 14, 52, 48, 104, DateTimeKind.Local).AddTicks(1845), "https://picsum.photos/640/480/?image=102", new DateTime(2020, 6, 20, 14, 52, 48, 104, DateTimeKind.Local).AddTicks(1888) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 20, 14, 52, 48, 104, DateTimeKind.Local).AddTicks(2054), "https://picsum.photos/640/480/?image=447", new DateTime(2020, 6, 20, 14, 52, 48, 104, DateTimeKind.Local).AddTicks(2093) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 20, 14, 52, 48, 104, DateTimeKind.Local).AddTicks(2256), "https://picsum.photos/640/480/?image=477", new DateTime(2020, 6, 20, 14, 52, 48, 104, DateTimeKind.Local).AddTicks(2297) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 20, 14, 52, 48, 104, DateTimeKind.Local).AddTicks(2462), "https://picsum.photos/640/480/?image=487", new DateTime(2020, 6, 20, 14, 52, 48, 104, DateTimeKind.Local).AddTicks(2504) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 20, 14, 52, 48, 104, DateTimeKind.Local).AddTicks(2669), "https://picsum.photos/640/480/?image=756", new DateTime(2020, 6, 20, 14, 52, 48, 104, DateTimeKind.Local).AddTicks(2707) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 20, 14, 52, 48, 104, DateTimeKind.Local).AddTicks(2868), "https://picsum.photos/640/480/?image=690", new DateTime(2020, 6, 20, 14, 52, 48, 104, DateTimeKind.Local).AddTicks(2910) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 20, 14, 52, 48, 104, DateTimeKind.Local).AddTicks(3071), "https://picsum.photos/640/480/?image=427", new DateTime(2020, 6, 20, 14, 52, 48, 104, DateTimeKind.Local).AddTicks(3110) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 20, 14, 52, 48, 104, DateTimeKind.Local).AddTicks(3270), "https://picsum.photos/640/480/?image=202", new DateTime(2020, 6, 20, 14, 52, 48, 104, DateTimeKind.Local).AddTicks(3313) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 20, 14, 52, 48, 104, DateTimeKind.Local).AddTicks(3480), "https://picsum.photos/640/480/?image=936", new DateTime(2020, 6, 20, 14, 52, 48, 104, DateTimeKind.Local).AddTicks(3521) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 20, 14, 52, 48, 104, DateTimeKind.Local).AddTicks(3684), "https://picsum.photos/640/480/?image=546", new DateTime(2020, 6, 20, 14, 52, 48, 104, DateTimeKind.Local).AddTicks(3725) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 20, 14, 52, 48, 104, DateTimeKind.Local).AddTicks(3888), "https://picsum.photos/640/480/?image=513", new DateTime(2020, 6, 20, 14, 52, 48, 104, DateTimeKind.Local).AddTicks(3929) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 20, 14, 52, 48, 104, DateTimeKind.Local).AddTicks(4089), "https://picsum.photos/640/480/?image=590", new DateTime(2020, 6, 20, 14, 52, 48, 104, DateTimeKind.Local).AddTicks(4128) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 20, 14, 52, 48, 104, DateTimeKind.Local).AddTicks(4558), "https://picsum.photos/640/480/?image=503", new DateTime(2020, 6, 20, 14, 52, 48, 104, DateTimeKind.Local).AddTicks(4618) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 20, 14, 52, 48, 104, DateTimeKind.Local).AddTicks(4814), "https://picsum.photos/640/480/?image=558", new DateTime(2020, 6, 20, 14, 52, 48, 104, DateTimeKind.Local).AddTicks(4855) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 20, 14, 52, 48, 104, DateTimeKind.Local).AddTicks(5028), "https://picsum.photos/640/480/?image=663", new DateTime(2020, 6, 20, 14, 52, 48, 104, DateTimeKind.Local).AddTicks(5069) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 20, 14, 52, 48, 104, DateTimeKind.Local).AddTicks(5231), "https://picsum.photos/640/480/?image=145", new DateTime(2020, 6, 20, 14, 52, 48, 104, DateTimeKind.Local).AddTicks(5271) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 20, 14, 52, 48, 104, DateTimeKind.Local).AddTicks(5434), "https://picsum.photos/640/480/?image=942", new DateTime(2020, 6, 20, 14, 52, 48, 104, DateTimeKind.Local).AddTicks(5477) });

            migrationBuilder.InsertData(
                table: "PostReactions",
                columns: new[] { "Id", "CreatedAt", "IsLike", "PostId", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 10, new DateTime(2020, 6, 20, 14, 52, 48, 755, DateTimeKind.Local).AddTicks(6546), false, 20, new DateTime(2020, 6, 20, 14, 52, 48, 755, DateTimeKind.Local).AddTicks(6579), 15 },
                    { 11, new DateTime(2020, 6, 20, 14, 52, 48, 755, DateTimeKind.Local).AddTicks(6744), false, 5, new DateTime(2020, 6, 20, 14, 52, 48, 755, DateTimeKind.Local).AddTicks(6778), 12 },
                    { 12, new DateTime(2020, 6, 20, 14, 52, 48, 755, DateTimeKind.Local).AddTicks(6941), true, 5, new DateTime(2020, 6, 20, 14, 52, 48, 755, DateTimeKind.Local).AddTicks(6976), 13 },
                    { 13, new DateTime(2020, 6, 20, 14, 52, 48, 755, DateTimeKind.Local).AddTicks(7137), false, 14, new DateTime(2020, 6, 20, 14, 52, 48, 755, DateTimeKind.Local).AddTicks(7171), 19 },
                    { 15, new DateTime(2020, 6, 20, 14, 52, 48, 755, DateTimeKind.Local).AddTicks(7527), true, 3, new DateTime(2020, 6, 20, 14, 52, 48, 755, DateTimeKind.Local).AddTicks(7563), 3 },
                    { 18, new DateTime(2020, 6, 20, 14, 52, 48, 755, DateTimeKind.Local).AddTicks(8118), false, 9, new DateTime(2020, 6, 20, 14, 52, 48, 755, DateTimeKind.Local).AddTicks(8152), 14 },
                    { 16, new DateTime(2020, 6, 20, 14, 52, 48, 755, DateTimeKind.Local).AddTicks(7722), false, 4, new DateTime(2020, 6, 20, 14, 52, 48, 755, DateTimeKind.Local).AddTicks(7756), 21 },
                    { 17, new DateTime(2020, 6, 20, 14, 52, 48, 755, DateTimeKind.Local).AddTicks(7916), true, 2, new DateTime(2020, 6, 20, 14, 52, 48, 755, DateTimeKind.Local).AddTicks(7950), 2 },
                    { 9, new DateTime(2020, 6, 20, 14, 52, 48, 755, DateTimeKind.Local).AddTicks(6351), false, 16, new DateTime(2020, 6, 20, 14, 52, 48, 755, DateTimeKind.Local).AddTicks(6387), 13 },
                    { 20, new DateTime(2020, 6, 20, 14, 52, 48, 755, DateTimeKind.Local).AddTicks(8662), true, 4, new DateTime(2020, 6, 20, 14, 52, 48, 755, DateTimeKind.Local).AddTicks(8700), 14 },
                    { 19, new DateTime(2020, 6, 20, 14, 52, 48, 755, DateTimeKind.Local).AddTicks(8455), true, 5, new DateTime(2020, 6, 20, 14, 52, 48, 755, DateTimeKind.Local).AddTicks(8493), 1 },
                    { 14, new DateTime(2020, 6, 20, 14, 52, 48, 755, DateTimeKind.Local).AddTicks(7330), true, 14, new DateTime(2020, 6, 20, 14, 52, 48, 755, DateTimeKind.Local).AddTicks(7366), 18 },
                    { 8, new DateTime(2020, 6, 20, 14, 52, 48, 755, DateTimeKind.Local).AddTicks(6154), false, 20, new DateTime(2020, 6, 20, 14, 52, 48, 755, DateTimeKind.Local).AddTicks(6189), 11 },
                    { 3, new DateTime(2020, 6, 20, 14, 52, 48, 755, DateTimeKind.Local).AddTicks(4973), false, 19, new DateTime(2020, 6, 20, 14, 52, 48, 755, DateTimeKind.Local).AddTicks(5038), 20 },
                    { 6, new DateTime(2020, 6, 20, 14, 52, 48, 755, DateTimeKind.Local).AddTicks(5753), false, 13, new DateTime(2020, 6, 20, 14, 52, 48, 755, DateTimeKind.Local).AddTicks(5789), 2 },
                    { 5, new DateTime(2020, 6, 20, 14, 52, 48, 755, DateTimeKind.Local).AddTicks(5484), true, 17, new DateTime(2020, 6, 20, 14, 52, 48, 755, DateTimeKind.Local).AddTicks(5520), 5 },
                    { 1, new DateTime(2020, 6, 20, 14, 52, 48, 754, DateTimeKind.Local).AddTicks(8083), false, 4, new DateTime(2020, 6, 20, 14, 52, 48, 755, DateTimeKind.Local).AddTicks(1946), 15 },
                    { 4, new DateTime(2020, 6, 20, 14, 52, 48, 755, DateTimeKind.Local).AddTicks(5267), false, 13, new DateTime(2020, 6, 20, 14, 52, 48, 755, DateTimeKind.Local).AddTicks(5305), 16 },
                    { 2, new DateTime(2020, 6, 20, 14, 52, 48, 755, DateTimeKind.Local).AddTicks(4467), true, 17, new DateTime(2020, 6, 20, 14, 52, 48, 755, DateTimeKind.Local).AddTicks(4570), 17 },
                    { 7, new DateTime(2020, 6, 20, 14, 52, 48, 755, DateTimeKind.Local).AddTicks(5960), true, 12, new DateTime(2020, 6, 20, 14, 52, 48, 755, DateTimeKind.Local).AddTicks(5994), 14 }
                });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 17, "Quo ea nostrum et voluptatem magni est.", new DateTime(2020, 6, 20, 14, 52, 48, 696, DateTimeKind.Local).AddTicks(3035), 24, new DateTime(2020, 6, 20, 14, 52, 48, 696, DateTimeKind.Local).AddTicks(7702) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 2, "Mollitia ut aut aliquam quis voluptatem soluta ut odio quisquam.", new DateTime(2020, 6, 20, 14, 52, 48, 697, DateTimeKind.Local).AddTicks(1517), 30, new DateTime(2020, 6, 20, 14, 52, 48, 697, DateTimeKind.Local).AddTicks(1649) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 19, "Molestias iure consequatur dignissimos voluptatem molestias est dicta. Qui inventore est. Voluptates labore illum omnis possimus corporis ut voluptatem. Accusamus et consequuntur et ut eos pariatur. Nisi vel sunt soluta dolorum. Autem voluptatem adipisci atque qui.", new DateTime(2020, 6, 20, 14, 52, 48, 699, DateTimeKind.Local).AddTicks(2190), 37, new DateTime(2020, 6, 20, 14, 52, 48, 699, DateTimeKind.Local).AddTicks(2336) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 21, @"Sit et velit et est.
Laborum officiis voluptatem tenetur occaecati.
Ipsam et est repudiandae ut possimus labore soluta.", new DateTime(2020, 6, 20, 14, 52, 48, 699, DateTimeKind.Local).AddTicks(8624), 33, new DateTime(2020, 6, 20, 14, 52, 48, 699, DateTimeKind.Local).AddTicks(8772) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 6, "Odio odio natus illum vitae quas soluta adipisci quo eum.", new DateTime(2020, 6, 20, 14, 52, 48, 699, DateTimeKind.Local).AddTicks(9423), 30, new DateTime(2020, 6, 20, 14, 52, 48, 699, DateTimeKind.Local).AddTicks(9468) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 4, @"Eaque voluptas numquam ea libero dolor harum.
Ut debitis et.
Sed et unde nam eaque laboriosam voluptatem.
Veniam maxime autem autem vel enim.
Voluptates atque ipsum suscipit magnam.
Eos velit repudiandae quam dolorum.", new DateTime(2020, 6, 20, 14, 52, 48, 700, DateTimeKind.Local).AddTicks(702), 33, new DateTime(2020, 6, 20, 14, 52, 48, 700, DateTimeKind.Local).AddTicks(758) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 5, "Itaque quia et. Voluptas nostrum neque dignissimos non in deleniti. Exercitationem aut nihil unde qui. Est occaecati quis perspiciatis et exercitationem.", new DateTime(2020, 6, 20, 14, 52, 48, 700, DateTimeKind.Local).AddTicks(1588), 40, new DateTime(2020, 6, 20, 14, 52, 48, 700, DateTimeKind.Local).AddTicks(1638) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 14, @"Quisquam odit distinctio pariatur quisquam at.
Saepe nihil natus et voluptatem est ea.
Consequatur et et earum asperiores architecto modi eligendi dicta.", new DateTime(2020, 6, 20, 14, 52, 48, 700, DateTimeKind.Local).AddTicks(2448), 39, new DateTime(2020, 6, 20, 14, 52, 48, 700, DateTimeKind.Local).AddTicks(2496) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 18, "Et qui non expedita sint accusantium non.", new DateTime(2020, 6, 20, 14, 52, 48, 700, DateTimeKind.Local).AddTicks(2905), 26, new DateTime(2020, 6, 20, 14, 52, 48, 700, DateTimeKind.Local).AddTicks(2950) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 3, "Sed aut et eligendi ut odit velit.", new DateTime(2020, 6, 20, 14, 52, 48, 700, DateTimeKind.Local).AddTicks(3348), 33, new DateTime(2020, 6, 20, 14, 52, 48, 700, DateTimeKind.Local).AddTicks(3393) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 4, "Voluptas eligendi magnam nisi. Velit in recusandae quasi praesentium aspernatur. Sed doloremque sapiente ea cumque. Error doloribus ipsam sit.", new DateTime(2020, 6, 20, 14, 52, 48, 700, DateTimeKind.Local).AddTicks(4197), 37, new DateTime(2020, 6, 20, 14, 52, 48, 700, DateTimeKind.Local).AddTicks(4239) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 5, "Autem reiciendis porro consequatur magnam quisquam qui.", new DateTime(2020, 6, 20, 14, 52, 48, 700, DateTimeKind.Local).AddTicks(4634), 29, new DateTime(2020, 6, 20, 14, 52, 48, 700, DateTimeKind.Local).AddTicks(4675) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 20, "Porro debitis dolore omnis et distinctio impedit iure. Officiis qui quibusdam rerum hic ut dolores. Asperiores accusamus veritatis et sapiente dolorum mollitia eius veritatis. Autem quia quo dolorem. Id perferendis voluptatum est maxime voluptas eum neque deleniti doloremque.", new DateTime(2020, 6, 20, 14, 52, 48, 700, DateTimeKind.Local).AddTicks(6058), 40, new DateTime(2020, 6, 20, 14, 52, 48, 700, DateTimeKind.Local).AddTicks(6114) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 3, "Ullam atque facilis. Quia sunt id omnis facere aliquam et. Odio qui suscipit voluptatem vero. Qui inventore non. Unde facere aspernatur neque impedit qui. Alias cumque possimus ullam repellat et necessitatibus deleniti ipsum dolorum.", new DateTime(2020, 6, 20, 14, 52, 48, 700, DateTimeKind.Local).AddTicks(7687), 39, new DateTime(2020, 6, 20, 14, 52, 48, 700, DateTimeKind.Local).AddTicks(7750) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 15, "Et sunt molestiae reprehenderit quo ratione assumenda aut.", new DateTime(2020, 6, 20, 14, 52, 48, 700, DateTimeKind.Local).AddTicks(8190), 38, new DateTime(2020, 6, 20, 14, 52, 48, 700, DateTimeKind.Local).AddTicks(8236) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 10, "Itaque necessitatibus dignissimos ipsa eum sit accusamus placeat iste. Aperiam odio perspiciatis dignissimos repellendus est molestiae ut molestiae cum. Sed distinctio tempore est repellat odio quia ut eum.", new DateTime(2020, 6, 20, 14, 52, 48, 700, DateTimeKind.Local).AddTicks(9086), 22, new DateTime(2020, 6, 20, 14, 52, 48, 700, DateTimeKind.Local).AddTicks(9134) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 18, "Officia est culpa cupiditate fugit eum aut debitis velit qui. Dolorem cumque voluptatibus velit ducimus praesentium itaque praesentium quasi qui. Est enim iure culpa.", new DateTime(2020, 6, 20, 14, 52, 48, 700, DateTimeKind.Local).AddTicks(9965), 35, new DateTime(2020, 6, 20, 14, 52, 48, 701, DateTimeKind.Local).AddTicks(10) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 4, @"Sunt laudantium quos velit delectus pariatur impedit.
Nostrum dolor et praesentium ad illum eum id qui.
Eum est quas ea perferendis magni.
Hic nemo eos sed quidem.
Voluptatem quis et.
Nihil quia repellendus dignissimos animi placeat.", new DateTime(2020, 6, 20, 14, 52, 48, 701, DateTimeKind.Local).AddTicks(1175), 23, new DateTime(2020, 6, 20, 14, 52, 48, 701, DateTimeKind.Local).AddTicks(1223) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 5, "Assumenda dolores qui iste voluptas id ex.", new DateTime(2020, 6, 20, 14, 52, 48, 701, DateTimeKind.Local).AddTicks(1634), 27, new DateTime(2020, 6, 20, 14, 52, 48, 701, DateTimeKind.Local).AddTicks(1676) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 8, "dolorem", new DateTime(2020, 6, 20, 14, 52, 48, 701, DateTimeKind.Local).AddTicks(6151), 30, new DateTime(2020, 6, 20, 14, 52, 48, 701, DateTimeKind.Local).AddTicks(6299) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 19, new DateTime(2020, 6, 20, 14, 52, 48, 234, DateTimeKind.Local).AddTicks(7370), "Shaniya48@hotmail.com", "IRfY+A4ypL6cR9CxRX7gWxnW4PM7UZHjWCrMTIQFrJs=", "mOL+IHlokgahB426xyo+FwcmKtoE4pYqupwVsQX9b94=", new DateTime(2020, 6, 20, 14, 52, 48, 235, DateTimeKind.Local).AddTicks(309), "Casimer60" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 9, new DateTime(2020, 6, 20, 14, 52, 48, 256, DateTimeKind.Local).AddTicks(9541), "Regan.Legros@hotmail.com", "CnbtaKff2wRIC8jUkPVkgwLq1nQK0tqlziYNThGB8N4=", "jbvVJJiFWixuecBFEUfwu6CktnaxuaJnmLkfFcubYN8=", new DateTime(2020, 6, 20, 14, 52, 48, 256, DateTimeKind.Local).AddTicks(9773), "Cortney_Sauer9" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 15, new DateTime(2020, 6, 20, 14, 52, 48, 277, DateTimeKind.Local).AddTicks(9419), "Maybell92@hotmail.com", "6BcWKIILp1KYCFV56SO7mYobvVaL6R75NAIAc2cEl7o=", "WqbqQh4H+Ykw6diuRo+nXyMUPVbMx9pqdAc6AcR+O78=", new DateTime(2020, 6, 20, 14, 52, 48, 277, DateTimeKind.Local).AddTicks(9576), "Laurie27" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 11, new DateTime(2020, 6, 20, 14, 52, 48, 297, DateTimeKind.Local).AddTicks(6977), "Carroll24@hotmail.com", "hmjs2WGxOolJJDExmuLBp4prkt8zBtDVTM4C0LuGZpo=", "n6BK+bxT8sMRFmqctgMVCO9n6tZSCw9q8H48OBZ/5/E=", new DateTime(2020, 6, 20, 14, 52, 48, 297, DateTimeKind.Local).AddTicks(7131), "Raquel87" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 5, new DateTime(2020, 6, 20, 14, 52, 48, 319, DateTimeKind.Local).AddTicks(8318), "Felton.Koepp@yahoo.com", "CSU9rS3StV/u/rsWwxaClE0tXTC3c0xHhxt8ob8PUz8=", "JwJ+tYUC9B5nEYz6Xk6IVZPxxSJmYLZtbt0pGdjZPec=", new DateTime(2020, 6, 20, 14, 52, 48, 319, DateTimeKind.Local).AddTicks(8477), "Carol_Rohan" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 3, new DateTime(2020, 6, 20, 14, 52, 48, 339, DateTimeKind.Local).AddTicks(6387), "Darrel.Quitzon0@hotmail.com", "2k/ZwcuGIhJynePYlU1J8jDsBBnEXEogDozunJ+HfPI=", "6gk8RGKhG9wKlYjZj+lFwIYNBwobu6sap7tFR3gIFxU=", new DateTime(2020, 6, 20, 14, 52, 48, 339, DateTimeKind.Local).AddTicks(6538), "Santa4" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 13, new DateTime(2020, 6, 20, 14, 52, 48, 360, DateTimeKind.Local).AddTicks(5696), "Brice.Reichert@gmail.com", "dUP9Ha0/vVnjkPP4OP4G2rC/cG/pao7odzTYZ0L4lo4=", "rmSZbF210oObNhEjGsIfTq22LxyEeMN8HjBJQbS7k8M=", new DateTime(2020, 6, 20, 14, 52, 48, 360, DateTimeKind.Local).AddTicks(6876), "Timmothy_Flatley" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 9, new DateTime(2020, 6, 20, 14, 52, 48, 384, DateTimeKind.Local).AddTicks(2117), "Jamil_Langworth@gmail.com", "DgVXnno/zhmOm+z+w5ZN/QY2cPuivLMDBtlGfpwLpcs=", "Gl64/DHuS56876FV5AunCvofbRcwzCnOFYqIq3U8SXE=", new DateTime(2020, 6, 20, 14, 52, 48, 384, DateTimeKind.Local).AddTicks(3378), "Adella_Macejkovic74" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 16, new DateTime(2020, 6, 20, 14, 52, 48, 406, DateTimeKind.Local).AddTicks(4037), "Sophia83@yahoo.com", "czaExVLCh4M21ouCvjONPF9TFNIhJO5bOGyegx8KbuU=", "6/XuQhRik+ZHCZI1d2E3Hn7dVBD5Bhdv4Acdi57IDJU=", new DateTime(2020, 6, 20, 14, 52, 48, 406, DateTimeKind.Local).AddTicks(5381), "Abraham.Cremin80" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 17, new DateTime(2020, 6, 20, 14, 52, 48, 426, DateTimeKind.Local).AddTicks(8646), "Brandy17@yahoo.com", "WyXKMPpqe80j9L/S1e+GxrJ3SJynQJhEMCiBEf2cjJk=", "wD32Mz8Hgf96rFkZvJJdtyKGRVPA8lsYDUgpztmTv6w=", new DateTime(2020, 6, 20, 14, 52, 48, 426, DateTimeKind.Local).AddTicks(8804), "Furman.Ledner96" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 5, new DateTime(2020, 6, 20, 14, 52, 48, 448, DateTimeKind.Local).AddTicks(6531), "Ronaldo.Auer@hotmail.com", "+PKQlhQ+IIA8afGLbFlfikyXdveq9KQ6XtqFiTv6U5M=", "PbJIeT8lNU8HEnpMThlMm/qC5LclEoF60j40TsGnAZE=", new DateTime(2020, 6, 20, 14, 52, 48, 448, DateTimeKind.Local).AddTicks(6685), "Baylee.Rodriguez" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 10, new DateTime(2020, 6, 20, 14, 52, 48, 468, DateTimeKind.Local).AddTicks(4597), "Ernestina.Kemmer@gmail.com", "AlmPwHaDvLwrhez3ferqIQLc5isKVRKnXiENvAxUzGw=", "Gipo8do+1DfSSy/MA9nKEUkrNCTl+8rsvKhFzEKxbXE=", new DateTime(2020, 6, 20, 14, 52, 48, 468, DateTimeKind.Local).AddTicks(4753), "Gianni96" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 17, new DateTime(2020, 6, 20, 14, 52, 48, 489, DateTimeKind.Local).AddTicks(341), "Sim_Marquardt@hotmail.com", "rEjCVjD7aezgCKyaALujwZMbs9UkYSh4bLm0oIkou0U=", "7NyYZ/6YZBBn42tkBe2UGxhTVkwChpsiEGQsgOMA4WA=", new DateTime(2020, 6, 20, 14, 52, 48, 489, DateTimeKind.Local).AddTicks(492), "Isac34" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { new DateTime(2020, 6, 20, 14, 52, 48, 509, DateTimeKind.Local).AddTicks(1983), "Shayna4@yahoo.com", "153OTYOdY/8w0vog8iSaRMvk3QatH5nXKm8F4Lnf91s=", "mEp807hqcHxmWLcFe+y8GWUsZnbfnYoO6JqnsyjF//w=", new DateTime(2020, 6, 20, 14, 52, 48, 509, DateTimeKind.Local).AddTicks(2142), "Winnifred88" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 11, new DateTime(2020, 6, 20, 14, 52, 48, 529, DateTimeKind.Local).AddTicks(2122), "Rocky6@yahoo.com", "4eSQJZwwWrhu3DDO6zQOVgwE2S2e97vuDX1dfd+R71o=", "sX/QOf70e88AscPE4eXykzjETCv9cgptq+1KPLSRKIc=", new DateTime(2020, 6, 20, 14, 52, 48, 529, DateTimeKind.Local).AddTicks(2280), "Greg_Adams48" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 4, new DateTime(2020, 6, 20, 14, 52, 48, 550, DateTimeKind.Local).AddTicks(7381), "Alexanne_Mraz@yahoo.com", "q6HpQwwElC6dOGhsvOEDbCxuY1OTO8IF3+In++3+uWc=", "gNaR73XRHXB+j9n9dt2LKYh8o4SOYAZuQL7AeKXCfYY=", new DateTime(2020, 6, 20, 14, 52, 48, 550, DateTimeKind.Local).AddTicks(7539), "Kendall93" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 19, new DateTime(2020, 6, 20, 14, 52, 48, 570, DateTimeKind.Local).AddTicks(6449), "Renee49@yahoo.com", "/W1MrWLfEhue9JzKHyaLugDja+6k35SAyoNGi2of/qk=", "3BwJPy4U/1PGe57WSl92y8/bCEbAp0hDnzI9NLet0ZM=", new DateTime(2020, 6, 20, 14, 52, 48, 570, DateTimeKind.Local).AddTicks(6604), "Frieda_Roob" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 9, new DateTime(2020, 6, 20, 14, 52, 48, 591, DateTimeKind.Local).AddTicks(8710), "Cale97@hotmail.com", "XvvYxxRI18qMf00X3p95wHHF7IG2u4OVvTA4PcuQPIo=", "6w8zxTllNnAA9bISN6pBybztzXJifNf4aA4+7QEukSA=", new DateTime(2020, 6, 20, 14, 52, 48, 591, DateTimeKind.Local).AddTicks(8865), "Jacey.Kiehn" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 8, new DateTime(2020, 6, 20, 14, 52, 48, 611, DateTimeKind.Local).AddTicks(8554), "Elena_Stehr@yahoo.com", "rIfXTFRA8+xqCvoac6/sdj4XV4YQmIXyNw2FqywaRKo=", "Z6aR3l1hNC7SwgCTlMdJ+S99R3IBHylv7bt0R53nQ5k=", new DateTime(2020, 6, 20, 14, 52, 48, 611, DateTimeKind.Local).AddTicks(9495), "Fred_Yost2" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 9, new DateTime(2020, 6, 20, 14, 52, 48, 635, DateTimeKind.Local).AddTicks(88), "Sadie.Bauch@gmail.com", "iaYkgWAF6WDMnE7gTYKMAVj17t/Jd5dMVodM3e/ZSAg=", "8po/JHzJPZNeadaXIBAV8XxMaLjc+fYRWY5yVr38BoY=", new DateTime(2020, 6, 20, 14, 52, 48, 635, DateTimeKind.Local).AddTicks(1038), "Mitchel21" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "Password", "Salt", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 20, 14, 52, 48, 654, DateTimeKind.Local).AddTicks(9557), "f6FaLQWAe8dZP89LeiAIoD+Bn1tKninq5yFsip0IFTA=", "TU4fC0jaEEeq/GyIWql46Shce3OXm5V7eU7DFUxo2Vg=", new DateTime(2020, 6, 20, 14, 52, 48, 654, DateTimeKind.Local).AddTicks(9557) });

            migrationBuilder.CreateIndex(
                name: "IX_ResetToken_UserId",
                table: "ResetToken",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ResetToken");

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.InsertData(
                table: "CommentReactions",
                columns: new[] { "Id", "CommentId", "CreatedAt", "IsLike", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 20, 16, new DateTime(2020, 6, 19, 9, 55, 59, 23, DateTimeKind.Local).AddTicks(4869), false, new DateTime(2020, 6, 19, 9, 55, 59, 23, DateTimeKind.Local).AddTicks(5033), 3 },
                    { 2, 20, new DateTime(2020, 6, 19, 9, 55, 59, 22, DateTimeKind.Local).AddTicks(7189), false, new DateTime(2020, 6, 19, 9, 55, 59, 22, DateTimeKind.Local).AddTicks(7439), 10 },
                    { 3, 5, new DateTime(2020, 6, 19, 9, 55, 59, 22, DateTimeKind.Local).AddTicks(8000), false, new DateTime(2020, 6, 19, 9, 55, 59, 22, DateTimeKind.Local).AddTicks(8047), 10 },
                    { 4, 12, new DateTime(2020, 6, 19, 9, 55, 59, 22, DateTimeKind.Local).AddTicks(8255), true, new DateTime(2020, 6, 19, 9, 55, 59, 22, DateTimeKind.Local).AddTicks(8299), 17 },
                    { 5, 5, new DateTime(2020, 6, 19, 9, 55, 59, 22, DateTimeKind.Local).AddTicks(8499), false, new DateTime(2020, 6, 19, 9, 55, 59, 22, DateTimeKind.Local).AddTicks(8542), 5 },
                    { 6, 4, new DateTime(2020, 6, 19, 9, 55, 59, 22, DateTimeKind.Local).AddTicks(8739), false, new DateTime(2020, 6, 19, 9, 55, 59, 22, DateTimeKind.Local).AddTicks(8783), 2 },
                    { 7, 9, new DateTime(2020, 6, 19, 9, 55, 59, 22, DateTimeKind.Local).AddTicks(8988), true, new DateTime(2020, 6, 19, 9, 55, 59, 22, DateTimeKind.Local).AddTicks(9032), 16 },
                    { 8, 13, new DateTime(2020, 6, 19, 9, 55, 59, 22, DateTimeKind.Local).AddTicks(9237), false, new DateTime(2020, 6, 19, 9, 55, 59, 22, DateTimeKind.Local).AddTicks(9279), 10 },
                    { 9, 16, new DateTime(2020, 6, 19, 9, 55, 59, 22, DateTimeKind.Local).AddTicks(9477), false, new DateTime(2020, 6, 19, 9, 55, 59, 22, DateTimeKind.Local).AddTicks(9902), 14 },
                    { 10, 4, new DateTime(2020, 6, 19, 9, 55, 59, 23, DateTimeKind.Local).AddTicks(211), false, new DateTime(2020, 6, 19, 9, 55, 59, 23, DateTimeKind.Local).AddTicks(262), 1 },
                    { 1, 13, new DateTime(2020, 6, 19, 9, 55, 59, 20, DateTimeKind.Local).AddTicks(5966), false, new DateTime(2020, 6, 19, 9, 55, 59, 21, DateTimeKind.Local).AddTicks(199), 2 },
                    { 12, 6, new DateTime(2020, 6, 19, 9, 55, 59, 23, DateTimeKind.Local).AddTicks(741), false, new DateTime(2020, 6, 19, 9, 55, 59, 23, DateTimeKind.Local).AddTicks(787), 6 },
                    { 13, 10, new DateTime(2020, 6, 19, 9, 55, 59, 23, DateTimeKind.Local).AddTicks(990), false, new DateTime(2020, 6, 19, 9, 55, 59, 23, DateTimeKind.Local).AddTicks(1034), 8 },
                    { 14, 3, new DateTime(2020, 6, 19, 9, 55, 59, 23, DateTimeKind.Local).AddTicks(1243), false, new DateTime(2020, 6, 19, 9, 55, 59, 23, DateTimeKind.Local).AddTicks(1290), 8 },
                    { 15, 4, new DateTime(2020, 6, 19, 9, 55, 59, 23, DateTimeKind.Local).AddTicks(1496), true, new DateTime(2020, 6, 19, 9, 55, 59, 23, DateTimeKind.Local).AddTicks(1542), 19 },
                    { 16, 11, new DateTime(2020, 6, 19, 9, 55, 59, 23, DateTimeKind.Local).AddTicks(1749), true, new DateTime(2020, 6, 19, 9, 55, 59, 23, DateTimeKind.Local).AddTicks(1794), 6 },
                    { 17, 18, new DateTime(2020, 6, 19, 9, 55, 59, 23, DateTimeKind.Local).AddTicks(1996), true, new DateTime(2020, 6, 19, 9, 55, 59, 23, DateTimeKind.Local).AddTicks(2044), 14 },
                    { 18, 19, new DateTime(2020, 6, 19, 9, 55, 59, 23, DateTimeKind.Local).AddTicks(2250), false, new DateTime(2020, 6, 19, 9, 55, 59, 23, DateTimeKind.Local).AddTicks(2297), 12 },
                    { 19, 13, new DateTime(2020, 6, 19, 9, 55, 59, 23, DateTimeKind.Local).AddTicks(2508), false, new DateTime(2020, 6, 19, 9, 55, 59, 23, DateTimeKind.Local).AddTicks(2552), 18 },
                    { 11, 17, new DateTime(2020, 6, 19, 9, 55, 59, 23, DateTimeKind.Local).AddTicks(483), true, new DateTime(2020, 6, 19, 9, 55, 59, 23, DateTimeKind.Local).AddTicks(530), 1 }
                });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 6, "Quis aut voluptate voluptatem exercitationem voluptas et assumenda magni.", new DateTime(2020, 6, 19, 9, 55, 58, 931, DateTimeKind.Local).AddTicks(8393), 1, new DateTime(2020, 6, 19, 9, 55, 58, 932, DateTimeKind.Local).AddTicks(3000) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 16, "Libero sed ut et.", new DateTime(2020, 6, 19, 9, 55, 58, 932, DateTimeKind.Local).AddTicks(6185), 11, new DateTime(2020, 6, 19, 9, 55, 58, 932, DateTimeKind.Local).AddTicks(6303) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 6, "Et et minus odio provident.", new DateTime(2020, 6, 19, 9, 55, 58, 932, DateTimeKind.Local).AddTicks(6797), 13, new DateTime(2020, 6, 19, 9, 55, 58, 932, DateTimeKind.Local).AddTicks(6848) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 16, "Beatae vitae perferendis nostrum.", new DateTime(2020, 6, 19, 9, 55, 58, 932, DateTimeKind.Local).AddTicks(7269), 1, new DateTime(2020, 6, 19, 9, 55, 58, 932, DateTimeKind.Local).AddTicks(7316) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 15, "Aspernatur recusandae saepe et.", new DateTime(2020, 6, 19, 9, 55, 58, 932, DateTimeKind.Local).AddTicks(7726), 16, new DateTime(2020, 6, 19, 9, 55, 58, 932, DateTimeKind.Local).AddTicks(7775) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 2, "Qui deleniti est et nisi autem non quas itaque.", new DateTime(2020, 6, 19, 9, 55, 58, 932, DateTimeKind.Local).AddTicks(8241), 18, new DateTime(2020, 6, 19, 9, 55, 58, 932, DateTimeKind.Local).AddTicks(8288) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 14, "Ut sequi quidem tempore debitis eaque.", new DateTime(2020, 6, 19, 9, 55, 58, 932, DateTimeKind.Local).AddTicks(8795), 4, new DateTime(2020, 6, 19, 9, 55, 58, 932, DateTimeKind.Local).AddTicks(8845) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 10, "Est fugit ut veniam incidunt.", new DateTime(2020, 6, 19, 9, 55, 58, 932, DateTimeKind.Local).AddTicks(9274), 3, new DateTime(2020, 6, 19, 9, 55, 58, 932, DateTimeKind.Local).AddTicks(9320) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 11, "Sed aut dolorem id ut.", new DateTime(2020, 6, 19, 9, 55, 58, 932, DateTimeKind.Local).AddTicks(9736), 6, new DateTime(2020, 6, 19, 9, 55, 58, 932, DateTimeKind.Local).AddTicks(9782) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 8, "Sint itaque quaerat eius sunt ratione recusandae reprehenderit.", new DateTime(2020, 6, 19, 9, 55, 58, 933, DateTimeKind.Local).AddTicks(1726), 14, new DateTime(2020, 6, 19, 9, 55, 58, 933, DateTimeKind.Local).AddTicks(1793) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 13, "Praesentium voluptatem sit.", new DateTime(2020, 6, 19, 9, 55, 58, 933, DateTimeKind.Local).AddTicks(2211), 1, new DateTime(2020, 6, 19, 9, 55, 58, 933, DateTimeKind.Local).AddTicks(2259) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 18, "Dignissimos qui commodi aut sit fugit earum.", new DateTime(2020, 6, 19, 9, 55, 58, 933, DateTimeKind.Local).AddTicks(2710), 5, new DateTime(2020, 6, 19, 9, 55, 58, 933, DateTimeKind.Local).AddTicks(2757) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 1, "Aut architecto dolor vitae libero eum.", new DateTime(2020, 6, 19, 9, 55, 58, 933, DateTimeKind.Local).AddTicks(3448), 20, new DateTime(2020, 6, 19, 9, 55, 58, 933, DateTimeKind.Local).AddTicks(3498) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "UpdatedAt" },
                values: new object[] { 12, "In a dolor.", new DateTime(2020, 6, 19, 9, 55, 58, 933, DateTimeKind.Local).AddTicks(3902), new DateTime(2020, 6, 19, 9, 55, 58, 933, DateTimeKind.Local).AddTicks(3949) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { "Rerum sunt ex facilis eaque sint corrupti.", new DateTime(2020, 6, 19, 9, 55, 58, 933, DateTimeKind.Local).AddTicks(4396), 1, new DateTime(2020, 6, 19, 9, 55, 58, 933, DateTimeKind.Local).AddTicks(4443) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 21, "Aperiam necessitatibus eos qui qui voluptatem qui quis maxime.", new DateTime(2020, 6, 19, 9, 55, 58, 933, DateTimeKind.Local).AddTicks(4909), 7, new DateTime(2020, 6, 19, 9, 55, 58, 933, DateTimeKind.Local).AddTicks(4955) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 14, "In velit architecto aliquam veritatis excepturi rerum.", new DateTime(2020, 6, 19, 9, 55, 58, 933, DateTimeKind.Local).AddTicks(5391), 14, new DateTime(2020, 6, 19, 9, 55, 58, 933, DateTimeKind.Local).AddTicks(5441) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "UpdatedAt" },
                values: new object[] { 16, "Sit tempora iure perspiciatis qui dolor neque aut corrupti quis.", new DateTime(2020, 6, 19, 9, 55, 58, 933, DateTimeKind.Local).AddTicks(5919), new DateTime(2020, 6, 19, 9, 55, 58, 933, DateTimeKind.Local).AddTicks(5970) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 5, "Dolores ratione quis minima vitae natus velit quibusdam.", new DateTime(2020, 6, 19, 9, 55, 58, 933, DateTimeKind.Local).AddTicks(6481), 6, new DateTime(2020, 6, 19, 9, 55, 58, 933, DateTimeKind.Local).AddTicks(6529) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 4, "Asperiores rerum sunt dolor blanditiis reiciendis.", new DateTime(2020, 6, 19, 9, 55, 58, 933, DateTimeKind.Local).AddTicks(6941), 9, new DateTime(2020, 6, 19, 9, 55, 58, 933, DateTimeKind.Local).AddTicks(6987) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 9, 55, 58, 247, DateTimeKind.Local).AddTicks(4081), "https://s3.amazonaws.com/uifaces/faces/twitter/doronmalki/128.jpg", new DateTime(2020, 6, 19, 9, 55, 58, 249, DateTimeKind.Local).AddTicks(6038) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 9, 55, 58, 249, DateTimeKind.Local).AddTicks(8971), "https://s3.amazonaws.com/uifaces/faces/twitter/mvdheuvel/128.jpg", new DateTime(2020, 6, 19, 9, 55, 58, 249, DateTimeKind.Local).AddTicks(9118) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 9, 55, 58, 249, DateTimeKind.Local).AddTicks(9368), "https://s3.amazonaws.com/uifaces/faces/twitter/tomas_janousek/128.jpg", new DateTime(2020, 6, 19, 9, 55, 58, 249, DateTimeKind.Local).AddTicks(9418) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 9, 55, 58, 249, DateTimeKind.Local).AddTicks(9625), "https://s3.amazonaws.com/uifaces/faces/twitter/n3dmax/128.jpg", new DateTime(2020, 6, 19, 9, 55, 58, 249, DateTimeKind.Local).AddTicks(9895) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 9, 55, 58, 250, DateTimeKind.Local).AddTicks(137), "https://s3.amazonaws.com/uifaces/faces/twitter/charliegann/128.jpg", new DateTime(2020, 6, 19, 9, 55, 58, 250, DateTimeKind.Local).AddTicks(181) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 9, 55, 58, 250, DateTimeKind.Local).AddTicks(375), "https://s3.amazonaws.com/uifaces/faces/twitter/nicolasfolliot/128.jpg", new DateTime(2020, 6, 19, 9, 55, 58, 250, DateTimeKind.Local).AddTicks(422) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 9, 55, 58, 250, DateTimeKind.Local).AddTicks(613), "https://s3.amazonaws.com/uifaces/faces/twitter/arindam_/128.jpg", new DateTime(2020, 6, 19, 9, 55, 58, 250, DateTimeKind.Local).AddTicks(657) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 9, 55, 58, 250, DateTimeKind.Local).AddTicks(840), "https://s3.amazonaws.com/uifaces/faces/twitter/zforrester/128.jpg", new DateTime(2020, 6, 19, 9, 55, 58, 250, DateTimeKind.Local).AddTicks(885) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 9, 55, 58, 250, DateTimeKind.Local).AddTicks(1072), "https://s3.amazonaws.com/uifaces/faces/twitter/sprayaga/128.jpg", new DateTime(2020, 6, 19, 9, 55, 58, 250, DateTimeKind.Local).AddTicks(1118) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 9, 55, 58, 250, DateTimeKind.Local).AddTicks(1303), "https://s3.amazonaws.com/uifaces/faces/twitter/jghyllebert/128.jpg", new DateTime(2020, 6, 19, 9, 55, 58, 250, DateTimeKind.Local).AddTicks(1349) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 9, 55, 58, 250, DateTimeKind.Local).AddTicks(1537), "https://s3.amazonaws.com/uifaces/faces/twitter/emsgulam/128.jpg", new DateTime(2020, 6, 19, 9, 55, 58, 250, DateTimeKind.Local).AddTicks(1581) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 9, 55, 58, 250, DateTimeKind.Local).AddTicks(1768), "https://s3.amazonaws.com/uifaces/faces/twitter/felipecsl/128.jpg", new DateTime(2020, 6, 19, 9, 55, 58, 250, DateTimeKind.Local).AddTicks(1812) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 9, 55, 58, 250, DateTimeKind.Local).AddTicks(1999), "https://s3.amazonaws.com/uifaces/faces/twitter/cloudstudio/128.jpg", new DateTime(2020, 6, 19, 9, 55, 58, 250, DateTimeKind.Local).AddTicks(2044) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 9, 55, 58, 250, DateTimeKind.Local).AddTicks(2233), "https://s3.amazonaws.com/uifaces/faces/twitter/sementiy/128.jpg", new DateTime(2020, 6, 19, 9, 55, 58, 250, DateTimeKind.Local).AddTicks(2280) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 9, 55, 58, 250, DateTimeKind.Local).AddTicks(2470), "https://s3.amazonaws.com/uifaces/faces/twitter/smaczny/128.jpg", new DateTime(2020, 6, 19, 9, 55, 58, 250, DateTimeKind.Local).AddTicks(2514) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 9, 55, 58, 250, DateTimeKind.Local).AddTicks(2770), "https://s3.amazonaws.com/uifaces/faces/twitter/edgarchris99/128.jpg", new DateTime(2020, 6, 19, 9, 55, 58, 250, DateTimeKind.Local).AddTicks(2820) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 9, 55, 58, 250, DateTimeKind.Local).AddTicks(3015), "https://s3.amazonaws.com/uifaces/faces/twitter/bluesix/128.jpg", new DateTime(2020, 6, 19, 9, 55, 58, 250, DateTimeKind.Local).AddTicks(3060) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 9, 55, 58, 250, DateTimeKind.Local).AddTicks(3247), "https://s3.amazonaws.com/uifaces/faces/twitter/sawalazar/128.jpg", new DateTime(2020, 6, 19, 9, 55, 58, 250, DateTimeKind.Local).AddTicks(3294) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 9, 55, 58, 250, DateTimeKind.Local).AddTicks(3486), "https://s3.amazonaws.com/uifaces/faces/twitter/kevinjohndayy/128.jpg", new DateTime(2020, 6, 19, 9, 55, 58, 250, DateTimeKind.Local).AddTicks(3534) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 9, 55, 58, 250, DateTimeKind.Local).AddTicks(3721), "https://s3.amazonaws.com/uifaces/faces/twitter/davidcazalis/128.jpg", new DateTime(2020, 6, 19, 9, 55, 58, 250, DateTimeKind.Local).AddTicks(3768) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 9, 55, 58, 270, DateTimeKind.Local).AddTicks(857), "https://picsum.photos/640/480/?image=176", new DateTime(2020, 6, 19, 9, 55, 58, 270, DateTimeKind.Local).AddTicks(5786) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 9, 55, 58, 270, DateTimeKind.Local).AddTicks(7337), "https://picsum.photos/640/480/?image=961", new DateTime(2020, 6, 19, 9, 55, 58, 270, DateTimeKind.Local).AddTicks(7490) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 9, 55, 58, 270, DateTimeKind.Local).AddTicks(7961), "https://picsum.photos/640/480/?image=1057", new DateTime(2020, 6, 19, 9, 55, 58, 270, DateTimeKind.Local).AddTicks(8009) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 9, 55, 58, 270, DateTimeKind.Local).AddTicks(8206), "https://picsum.photos/640/480/?image=1011", new DateTime(2020, 6, 19, 9, 55, 58, 270, DateTimeKind.Local).AddTicks(8250) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 9, 55, 58, 270, DateTimeKind.Local).AddTicks(8428), "https://picsum.photos/640/480/?image=370", new DateTime(2020, 6, 19, 9, 55, 58, 270, DateTimeKind.Local).AddTicks(8472) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 9, 55, 58, 270, DateTimeKind.Local).AddTicks(8648), "https://picsum.photos/640/480/?image=471", new DateTime(2020, 6, 19, 9, 55, 58, 270, DateTimeKind.Local).AddTicks(8690) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 9, 55, 58, 270, DateTimeKind.Local).AddTicks(8864), "https://picsum.photos/640/480/?image=1007", new DateTime(2020, 6, 19, 9, 55, 58, 270, DateTimeKind.Local).AddTicks(8902) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 9, 55, 58, 270, DateTimeKind.Local).AddTicks(9074), "https://picsum.photos/640/480/?image=183", new DateTime(2020, 6, 19, 9, 55, 58, 270, DateTimeKind.Local).AddTicks(9116) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 9, 55, 58, 270, DateTimeKind.Local).AddTicks(9291), "https://picsum.photos/640/480/?image=161", new DateTime(2020, 6, 19, 9, 55, 58, 270, DateTimeKind.Local).AddTicks(9333) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 9, 55, 58, 270, DateTimeKind.Local).AddTicks(9508), "https://picsum.photos/640/480/?image=933", new DateTime(2020, 6, 19, 9, 55, 58, 271, DateTimeKind.Local).AddTicks(232) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 9, 55, 58, 271, DateTimeKind.Local).AddTicks(471), "https://picsum.photos/640/480/?image=719", new DateTime(2020, 6, 19, 9, 55, 58, 271, DateTimeKind.Local).AddTicks(518) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 9, 55, 58, 271, DateTimeKind.Local).AddTicks(705), "https://picsum.photos/640/480/?image=82", new DateTime(2020, 6, 19, 9, 55, 58, 271, DateTimeKind.Local).AddTicks(747) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 9, 55, 58, 271, DateTimeKind.Local).AddTicks(928), "https://picsum.photos/640/480/?image=695", new DateTime(2020, 6, 19, 9, 55, 58, 271, DateTimeKind.Local).AddTicks(971) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 9, 55, 58, 271, DateTimeKind.Local).AddTicks(1152), "https://picsum.photos/640/480/?image=1025", new DateTime(2020, 6, 19, 9, 55, 58, 271, DateTimeKind.Local).AddTicks(1194) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 9, 55, 58, 271, DateTimeKind.Local).AddTicks(1371), "https://picsum.photos/640/480/?image=1059", new DateTime(2020, 6, 19, 9, 55, 58, 271, DateTimeKind.Local).AddTicks(1412) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 9, 55, 58, 271, DateTimeKind.Local).AddTicks(1654), "https://picsum.photos/640/480/?image=452", new DateTime(2020, 6, 19, 9, 55, 58, 271, DateTimeKind.Local).AddTicks(1699) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 9, 55, 58, 271, DateTimeKind.Local).AddTicks(1873), "https://picsum.photos/640/480/?image=978", new DateTime(2020, 6, 19, 9, 55, 58, 271, DateTimeKind.Local).AddTicks(1917) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 9, 55, 58, 271, DateTimeKind.Local).AddTicks(2096), "https://picsum.photos/640/480/?image=251", new DateTime(2020, 6, 19, 9, 55, 58, 271, DateTimeKind.Local).AddTicks(2138) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 9, 55, 58, 271, DateTimeKind.Local).AddTicks(2312), "https://picsum.photos/640/480/?image=728", new DateTime(2020, 6, 19, 9, 55, 58, 271, DateTimeKind.Local).AddTicks(2351) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 9, 55, 58, 271, DateTimeKind.Local).AddTicks(2530), "https://picsum.photos/640/480/?image=1016", new DateTime(2020, 6, 19, 9, 55, 58, 271, DateTimeKind.Local).AddTicks(2575) });

            migrationBuilder.InsertData(
                table: "PostReactions",
                columns: new[] { "Id", "CreatedAt", "IsLike", "PostId", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 10, new DateTime(2020, 6, 19, 9, 55, 58, 977, DateTimeKind.Local).AddTicks(6109), false, 8, new DateTime(2020, 6, 19, 9, 55, 58, 977, DateTimeKind.Local).AddTicks(6153), 13 },
                    { 11, new DateTime(2020, 6, 19, 9, 55, 58, 977, DateTimeKind.Local).AddTicks(6357), true, 3, new DateTime(2020, 6, 19, 9, 55, 58, 977, DateTimeKind.Local).AddTicks(6398), 11 },
                    { 12, new DateTime(2020, 6, 19, 9, 55, 58, 977, DateTimeKind.Local).AddTicks(6609), true, 11, new DateTime(2020, 6, 19, 9, 55, 58, 977, DateTimeKind.Local).AddTicks(6653), 2 },
                    { 13, new DateTime(2020, 6, 19, 9, 55, 58, 977, DateTimeKind.Local).AddTicks(6865), false, 4, new DateTime(2020, 6, 19, 9, 55, 58, 977, DateTimeKind.Local).AddTicks(6910), 11 },
                    { 15, new DateTime(2020, 6, 19, 9, 55, 58, 977, DateTimeKind.Local).AddTicks(7365), true, 15, new DateTime(2020, 6, 19, 9, 55, 58, 977, DateTimeKind.Local).AddTicks(7406), 20 },
                    { 18, new DateTime(2020, 6, 19, 9, 55, 58, 977, DateTimeKind.Local).AddTicks(8106), false, 12, new DateTime(2020, 6, 19, 9, 55, 58, 977, DateTimeKind.Local).AddTicks(8153), 16 },
                    { 16, new DateTime(2020, 6, 19, 9, 55, 58, 977, DateTimeKind.Local).AddTicks(7608), true, 14, new DateTime(2020, 6, 19, 9, 55, 58, 977, DateTimeKind.Local).AddTicks(7652), 13 },
                    { 17, new DateTime(2020, 6, 19, 9, 55, 58, 977, DateTimeKind.Local).AddTicks(7858), true, 14, new DateTime(2020, 6, 19, 9, 55, 58, 977, DateTimeKind.Local).AddTicks(7901), 21 },
                    { 9, new DateTime(2020, 6, 19, 9, 55, 58, 977, DateTimeKind.Local).AddTicks(5860), false, 12, new DateTime(2020, 6, 19, 9, 55, 58, 977, DateTimeKind.Local).AddTicks(5905), 21 },
                    { 20, new DateTime(2020, 6, 19, 9, 55, 58, 977, DateTimeKind.Local).AddTicks(8614), false, 5, new DateTime(2020, 6, 19, 9, 55, 58, 977, DateTimeKind.Local).AddTicks(8659), 14 },
                    { 19, new DateTime(2020, 6, 19, 9, 55, 58, 977, DateTimeKind.Local).AddTicks(8358), true, 1, new DateTime(2020, 6, 19, 9, 55, 58, 977, DateTimeKind.Local).AddTicks(8402), 20 },
                    { 14, new DateTime(2020, 6, 19, 9, 55, 58, 977, DateTimeKind.Local).AddTicks(7116), true, 7, new DateTime(2020, 6, 19, 9, 55, 58, 977, DateTimeKind.Local).AddTicks(7161), 8 },
                    { 8, new DateTime(2020, 6, 19, 9, 55, 58, 977, DateTimeKind.Local).AddTicks(5620), true, 20, new DateTime(2020, 6, 19, 9, 55, 58, 977, DateTimeKind.Local).AddTicks(5662), 3 },
                    { 3, new DateTime(2020, 6, 19, 9, 55, 58, 977, DateTimeKind.Local).AddTicks(4338), false, 5, new DateTime(2020, 6, 19, 9, 55, 58, 977, DateTimeKind.Local).AddTicks(4388), 5 },
                    { 6, new DateTime(2020, 6, 19, 9, 55, 58, 977, DateTimeKind.Local).AddTicks(5119), false, 14, new DateTime(2020, 6, 19, 9, 55, 58, 977, DateTimeKind.Local).AddTicks(5166), 3 },
                    { 5, new DateTime(2020, 6, 19, 9, 55, 58, 977, DateTimeKind.Local).AddTicks(4865), false, 16, new DateTime(2020, 6, 19, 9, 55, 58, 977, DateTimeKind.Local).AddTicks(4909), 7 },
                    { 1, new DateTime(2020, 6, 19, 9, 55, 58, 974, DateTimeKind.Local).AddTicks(9365), true, 15, new DateTime(2020, 6, 19, 9, 55, 58, 977, DateTimeKind.Local).AddTicks(1016), 3 },
                    { 4, new DateTime(2020, 6, 19, 9, 55, 58, 977, DateTimeKind.Local).AddTicks(4615), false, 1, new DateTime(2020, 6, 19, 9, 55, 58, 977, DateTimeKind.Local).AddTicks(4660), 10 },
                    { 2, new DateTime(2020, 6, 19, 9, 55, 58, 977, DateTimeKind.Local).AddTicks(3968), false, 6, new DateTime(2020, 6, 19, 9, 55, 58, 977, DateTimeKind.Local).AddTicks(4076), 5 },
                    { 7, new DateTime(2020, 6, 19, 9, 55, 58, 977, DateTimeKind.Local).AddTicks(5379), true, 2, new DateTime(2020, 6, 19, 9, 55, 58, 977, DateTimeKind.Local).AddTicks(5421), 12 }
                });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 6, @"Quo voluptatem dolor aut sit quibusdam tempora.
Et vel ea non non voluptatum perferendis.
Ab eaque facilis aspernatur et explicabo magni quidem odio aut.
Officiis veniam nemo.", new DateTime(2020, 6, 19, 9, 55, 58, 900, DateTimeKind.Local).AddTicks(9585), 36, new DateTime(2020, 6, 19, 9, 55, 58, 901, DateTimeKind.Local).AddTicks(4938) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 20, "Labore tenetur fugit deleniti. Officiis similique enim officia est ab aut. Dolor corporis sed enim quae blanditiis optio et magni. Libero et dolor aliquid.", new DateTime(2020, 6, 19, 9, 55, 58, 902, DateTimeKind.Local).AddTicks(6790), 26, new DateTime(2020, 6, 19, 9, 55, 58, 902, DateTimeKind.Local).AddTicks(6998) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 16, "neque", new DateTime(2020, 6, 19, 9, 55, 58, 903, DateTimeKind.Local).AddTicks(1252), 36, new DateTime(2020, 6, 19, 9, 55, 58, 903, DateTimeKind.Local).AddTicks(1402) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 3, "Eaque recusandae natus dolores tempora ex repellendus ut doloremque.", new DateTime(2020, 6, 19, 9, 55, 58, 903, DateTimeKind.Local).AddTicks(6205), 28, new DateTime(2020, 6, 19, 9, 55, 58, 903, DateTimeKind.Local).AddTicks(6317) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 17, "explicabo", new DateTime(2020, 6, 19, 9, 55, 58, 903, DateTimeKind.Local).AddTicks(6723), 40, new DateTime(2020, 6, 19, 9, 55, 58, 903, DateTimeKind.Local).AddTicks(6774) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 10, "Ullam assumenda molestias odit.", new DateTime(2020, 6, 19, 9, 55, 58, 903, DateTimeKind.Local).AddTicks(7230), 37, new DateTime(2020, 6, 19, 9, 55, 58, 903, DateTimeKind.Local).AddTicks(7280) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 3, @"Enim autem error dignissimos minima voluptatem corrupti.
Voluptatem animi placeat.
Incidunt laboriosam fugiat officia fugit minima aut id aliquam ad.
Unde ut rem reiciendis.
Tempore incidunt corrupti maiores ea nihil ipsa voluptate reiciendis.", new DateTime(2020, 6, 19, 9, 55, 58, 903, DateTimeKind.Local).AddTicks(8605), 32, new DateTime(2020, 6, 19, 9, 55, 58, 903, DateTimeKind.Local).AddTicks(8659) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 12, "Porro dolorem est. Quasi et cum sint eligendi. Assumenda quia voluptatem eaque. Perferendis voluptatum consequatur quis. Sit aperiam fugit voluptas molestias non exercitationem molestiae.", new DateTime(2020, 6, 19, 9, 55, 58, 903, DateTimeKind.Local).AddTicks(9783), 33, new DateTime(2020, 6, 19, 9, 55, 58, 904, DateTimeKind.Local).AddTicks(93) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 4, "delectus", new DateTime(2020, 6, 19, 9, 55, 58, 904, DateTimeKind.Local).AddTicks(467), 39, new DateTime(2020, 6, 19, 9, 55, 58, 904, DateTimeKind.Local).AddTicks(518) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 7, @"Est in perspiciatis dolorem sequi sed quia eius veniam.
Consequatur adipisci delectus odit exercitationem omnis.
Possimus soluta illo.
Est ut dolores dolore et neque asperiores et quia aspernatur.
Numquam consectetur temporibus velit quod atque.
Dolores temporibus perspiciatis tempora.", new DateTime(2020, 6, 19, 9, 55, 58, 904, DateTimeKind.Local).AddTicks(1949), 22, new DateTime(2020, 6, 19, 9, 55, 58, 904, DateTimeKind.Local).AddTicks(2002) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 19, "facilis", new DateTime(2020, 6, 19, 9, 55, 58, 904, DateTimeKind.Local).AddTicks(2318), 29, new DateTime(2020, 6, 19, 9, 55, 58, 904, DateTimeKind.Local).AddTicks(2367) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 9, "Velit debitis earum eos sint vero voluptate sint magnam autem.", new DateTime(2020, 6, 19, 9, 55, 58, 904, DateTimeKind.Local).AddTicks(2863), 27, new DateTime(2020, 6, 19, 9, 55, 58, 904, DateTimeKind.Local).AddTicks(2914) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 21, "nisi", new DateTime(2020, 6, 19, 9, 55, 58, 904, DateTimeKind.Local).AddTicks(3263), 31, new DateTime(2020, 6, 19, 9, 55, 58, 904, DateTimeKind.Local).AddTicks(3317) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 17, "Consequatur iste culpa amet ut quam perspiciatis qui nulla sit. Id fugit iure sit nobis. Ad voluptas officiis omnis sit similique molestiae. Praesentium magni sequi.", new DateTime(2020, 6, 19, 9, 55, 58, 904, DateTimeKind.Local).AddTicks(4482), 35, new DateTime(2020, 6, 19, 9, 55, 58, 904, DateTimeKind.Local).AddTicks(4540) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 12, "Odit doloribus id voluptatem fugiat eligendi tempore dignissimos quis.", new DateTime(2020, 6, 19, 9, 55, 58, 904, DateTimeKind.Local).AddTicks(5089), 29, new DateTime(2020, 6, 19, 9, 55, 58, 904, DateTimeKind.Local).AddTicks(5143) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 9, "Numquam accusantium quod expedita molestiae nihil qui ducimus. Voluptatum doloribus et et aut. Reprehenderit nihil dicta voluptas qui. Consequuntur repellendus perferendis at et animi rerum et. Assumenda libero itaque deleniti. Odit velit sunt quo.", new DateTime(2020, 6, 19, 9, 55, 58, 904, DateTimeKind.Local).AddTicks(6483), 31, new DateTime(2020, 6, 19, 9, 55, 58, 904, DateTimeKind.Local).AddTicks(6539) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 21, @"Sit sit et et.
Reiciendis cumque voluptatum magni.
Consequatur sunt et qui aut sit aut.
Rerum consectetur nihil voluptatem quo qui error.
Voluptatibus et dolore eius.", new DateTime(2020, 6, 19, 9, 55, 58, 904, DateTimeKind.Local).AddTicks(7623), 40, new DateTime(2020, 6, 19, 9, 55, 58, 904, DateTimeKind.Local).AddTicks(7676) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 8, "Soluta libero minima minus qui cupiditate dignissimos exercitationem.", new DateTime(2020, 6, 19, 9, 55, 58, 904, DateTimeKind.Local).AddTicks(8199), 31, new DateTime(2020, 6, 19, 9, 55, 58, 904, DateTimeKind.Local).AddTicks(8247) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 18, "Eos nobis quis illum.", new DateTime(2020, 6, 19, 9, 55, 58, 904, DateTimeKind.Local).AddTicks(8690), 29, new DateTime(2020, 6, 19, 9, 55, 58, 904, DateTimeKind.Local).AddTicks(8740) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 9, @"Perferendis totam ipsa corporis aut repellat.
Incidunt repudiandae laborum.
Dolorum qui quae.
Quod consequatur quo nihil.", new DateTime(2020, 6, 19, 9, 55, 58, 904, DateTimeKind.Local).AddTicks(9630), 35, new DateTime(2020, 6, 19, 9, 55, 58, 904, DateTimeKind.Local).AddTicks(9682) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 14, new DateTime(2020, 6, 19, 9, 55, 58, 405, DateTimeKind.Local).AddTicks(7089), "Floyd_Collier@gmail.com", "7LDPUZHea3zyulk1Ta7bK/jefL0yy9el9AU5aXKv7dE=", "PCs5p++3SqbHn6o8mQdTkykVt3rttDZQz5AMWb/pZ5s=", new DateTime(2020, 6, 19, 9, 55, 58, 406, DateTimeKind.Local).AddTicks(4610), "Bradley30" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 6, new DateTime(2020, 6, 19, 9, 55, 58, 427, DateTimeKind.Local).AddTicks(2883), "Marcia.Spencer51@yahoo.com", "pV8vq5HMrdvYn4+0+CX6OG7s2V/eeMJMzDzSsoZZoAg=", "aAm1/JIH8N4nDIvMSXmBHzx+9ziqi0wCRzkcjfdyUiY=", new DateTime(2020, 6, 19, 9, 55, 58, 427, DateTimeKind.Local).AddTicks(3101), "Clyde_Tillman93" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 19, new DateTime(2020, 6, 19, 9, 55, 58, 446, DateTimeKind.Local).AddTicks(9587), "Genoveva.Cormier@hotmail.com", "7zG0ej9IfsWpl9aTj0WuUHbjYH90w4CQ+1XYT+ifXS8=", "EK6OshTpwuXtm9Lc17MULIvJNf0yhLXw05wNYaUlwtQ=", new DateTime(2020, 6, 19, 9, 55, 58, 446, DateTimeKind.Local).AddTicks(9865), "Piper.Jacobi49" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 8, new DateTime(2020, 6, 19, 9, 55, 58, 469, DateTimeKind.Local).AddTicks(8813), "Julio.Kulas@gmail.com", "Pv9dhISpDLffMCpLlI85/BycBSSJGRIhTr+CyA09PFM=", "a08fJvFD/62+zBUxcdrfZZA7N9aDZOfl1hilcmWGmrM=", new DateTime(2020, 6, 19, 9, 55, 58, 469, DateTimeKind.Local).AddTicks(8969), "Corbin60" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 2, new DateTime(2020, 6, 19, 9, 55, 58, 491, DateTimeKind.Local).AddTicks(86), "Katelyn56@hotmail.com", "Gro5Kv6+DTyF/x7k6LkCnJ7zAy4BZ9Z7n8QFxBbw+E4=", "zVDu9fY7zEiRbCGzLrihtUvuFFdvHEEfXauyKnEcKzY=", new DateTime(2020, 6, 19, 9, 55, 58, 491, DateTimeKind.Local).AddTicks(271), "Adriel.Rolfson" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 14, new DateTime(2020, 6, 19, 9, 55, 58, 513, DateTimeKind.Local).AddTicks(2558), "Ted77@hotmail.com", "+0TajNAnfzwmisWVdzGFt2oGgh62GkjRUL+twZSx+Po=", "N0/QyqPTZOSAd6WMkBEIcBnghRiE/07MWYRo7IG1M7w=", new DateTime(2020, 6, 19, 9, 55, 58, 513, DateTimeKind.Local).AddTicks(2713), "Velda.Tillman" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 17, new DateTime(2020, 6, 19, 9, 55, 58, 533, DateTimeKind.Local).AddTicks(4874), "Doyle.Kuphal@hotmail.com", "cHKLnk2+V0d7hVuJ4zyaxhNiM+f0hAt//pobpa4oFwg=", "TrwYH0WyLpBERIF5/rlgs0ZpV6s1Zrr1MUpz0SYS0ig=", new DateTime(2020, 6, 19, 9, 55, 58, 533, DateTimeKind.Local).AddTicks(5036), "Jeffrey_Erdman30" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 20, new DateTime(2020, 6, 19, 9, 55, 58, 558, DateTimeKind.Local).AddTicks(1169), "Elda89@yahoo.com", "GXgihixUqGLw4dEiFIpElD6rSeUzgW9Jl8zRpWlcfUQ=", "icCr+Os5e12odDpm1sBZhioGTUDCUXmTFPF729xHe0g=", new DateTime(2020, 6, 19, 9, 55, 58, 558, DateTimeKind.Local).AddTicks(2178), "Hubert.Price" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 5, new DateTime(2020, 6, 19, 9, 55, 58, 583, DateTimeKind.Local).AddTicks(3687), "Genevieve.Veum@yahoo.com", "d0mmbEuzmBd5tB4uW9uUmBWHOim2Wifb08nXdn45qCg=", "HqQw8jHVvZPqXviwGAYcQhjPUwFF/qgO6vZBzuS+Who=", new DateTime(2020, 6, 19, 9, 55, 58, 583, DateTimeKind.Local).AddTicks(4837), "Flavie9" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 11, new DateTime(2020, 6, 19, 9, 55, 58, 607, DateTimeKind.Local).AddTicks(4082), "Kyla.Howe@yahoo.com", "UMulSphldMlfEutbZ/r4V43znuJ+43bRHFqqNTDFXsQ=", "PyTDqVvBIVqzaunYcQNicUJTPBVdwrNBeC/P1Wv4qsI=", new DateTime(2020, 6, 19, 9, 55, 58, 607, DateTimeKind.Local).AddTicks(4882), "Scarlett_Hills" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 12, new DateTime(2020, 6, 19, 9, 55, 58, 628, DateTimeKind.Local).AddTicks(5598), "Beatrice55@hotmail.com", "AX/VIJgoDVVaVqHS4tMtMzVhafvua0AlraY6+aD8kCk=", "esb9N0cF9dPAhWLbGZ/ND9WCR4hKM0kxzW7O7aFx7Dg=", new DateTime(2020, 6, 19, 9, 55, 58, 628, DateTimeKind.Local).AddTicks(5753), "Kole_Zieme62" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 9, new DateTime(2020, 6, 19, 9, 55, 58, 650, DateTimeKind.Local).AddTicks(4329), "Arnold.Becker63@yahoo.com", "EcQa21MqrrEZmbSVAHbVVW9cMmYtGwa1UePpSR0Qgko=", "c2RBNhbQ1PsGzFoKHYlnTdMLRi1fN7+mxoHnMknKFPA=", new DateTime(2020, 6, 19, 9, 55, 58, 650, DateTimeKind.Local).AddTicks(4480), "Grayson.Gleason28" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 16, new DateTime(2020, 6, 19, 9, 55, 58, 672, DateTimeKind.Local).AddTicks(86), "Candice.Smith50@gmail.com", "RUykHwwd0iQYoZUL3zoYPGKQI2kF0fPSTKzJNRABS5M=", "T+srpz533k7xWskJxQkVmR3cDhHdILrheW6lH6oif1w=", new DateTime(2020, 6, 19, 9, 55, 58, 672, DateTimeKind.Local).AddTicks(247), "Alyce.Marquardt82" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { new DateTime(2020, 6, 19, 9, 55, 58, 696, DateTimeKind.Local).AddTicks(4965), "Sigurd_Hamill80@gmail.com", "9TTew1z1q1oHxbFW3nHiD2mDAeGA1BWRvYOWb+tYYuw=", "9kFcDp3aaTA2yY0rBaeDGbTLX+LhCYmC8My0sgiwHyw=", new DateTime(2020, 6, 19, 9, 55, 58, 696, DateTimeKind.Local).AddTicks(5121), "Vivian.Collins" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 12, new DateTime(2020, 6, 19, 9, 55, 58, 717, DateTimeKind.Local).AddTicks(7246), "Shea71@yahoo.com", "czHoApC2Oww13XgJ71BmikdDLBEX/FxgrgprAHUY9QI=", "3BKjQ62Ylls9Vc33+b7yqRo1Wi4xgkTFybhzuglAd+g=", new DateTime(2020, 6, 19, 9, 55, 58, 717, DateTimeKind.Local).AddTicks(7410), "Modesto.Marquardt18" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 7, new DateTime(2020, 6, 19, 9, 55, 58, 740, DateTimeKind.Local).AddTicks(7489), "Clair.Gislason8@hotmail.com", "8rm3iolcM211dZwtIZxzBf4tnWLo5Wr7iI6KDjhHr/A=", "9QOLNRzERU/NX/ZRTLTMOxVHLSQSGgyu3UAngiyYpfo=", new DateTime(2020, 6, 19, 9, 55, 58, 740, DateTimeKind.Local).AddTicks(7638), "Ila16" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 13, new DateTime(2020, 6, 19, 9, 55, 58, 763, DateTimeKind.Local).AddTicks(8241), "Donnell_Balistreri69@yahoo.com", "hMQ9BgZ80SidT90KDyVsZF2mqlSGGvQJLRR0mHkP2vY=", "eoqzu7uap5EMVzAT/qZpnKsX6ZMpHV0w953ItiBsF8w=", new DateTime(2020, 6, 19, 9, 55, 58, 763, DateTimeKind.Local).AddTicks(8396), "Kari91" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 7, new DateTime(2020, 6, 19, 9, 55, 58, 784, DateTimeKind.Local).AddTicks(5554), "Sedrick_Abernathy@yahoo.com", "RRPEy34PReX98boepiYeZCUN2q9kjCSpFKYmoJ2N9cM=", "INH8NSbnmo0a9DH6tr6560DlNTQx3xPeZ7H9TIZfhK4=", new DateTime(2020, 6, 19, 9, 55, 58, 784, DateTimeKind.Local).AddTicks(5713), "Gerry24" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 1, new DateTime(2020, 6, 19, 9, 55, 58, 811, DateTimeKind.Local).AddTicks(5087), "Aidan_Stoltenberg74@gmail.com", "I5uFOT9cGm68No/7f9vkC3+qBjwnzLz0QMJfnnAIdno=", "X+s6nGaaPQaHmKMfYm15KO45vqOioqG8CLbAS3Uoagc=", new DateTime(2020, 6, 19, 9, 55, 58, 811, DateTimeKind.Local).AddTicks(5239), "Felicita_Jones" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 7, new DateTime(2020, 6, 19, 9, 55, 58, 833, DateTimeKind.Local).AddTicks(4490), "Erin_Osinski@yahoo.com", "VzUEFT7CyMGVlymK2NnTY2lvJGPNXUfRrjnMq7O3Fc4=", "kUE1J2HQlJkUfR84+XmICJHAwLj3SvdpXJ0FjoISU8s=", new DateTime(2020, 6, 19, 9, 55, 58, 833, DateTimeKind.Local).AddTicks(4640), "Lulu.Lehner" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "Password", "Salt", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 19, 9, 55, 58, 855, DateTimeKind.Local).AddTicks(7251), "wttBlnTSRx+CD1TqRVmwzKnk3XsjEDDZwjn7ol2qzr0=", "N5NodQAj3ieNzf91ileTLxV8QfJetR8BrlulecTq3VA=", new DateTime(2020, 6, 19, 9, 55, 58, 855, DateTimeKind.Local).AddTicks(7251) });
        }
    }
}
